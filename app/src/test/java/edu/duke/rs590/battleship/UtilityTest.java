package edu.duke.rs590.battleship;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.parallel.ResourceAccessMode;
import org.junit.jupiter.api.parallel.ResourceLock;
import org.junit.jupiter.api.parallel.Resources;

import java.io.*;
import java.util.Random;
import java.util.TreeSet;

import static org.junit.jupiter.api.Assertions.*;

class UtilityTest {
    private final Random randomGenerator = new Random();

    @Test
    void test_read_from_resources() {
        // valid
        String actual = Constants.PROMPT_PLACEMENT;
        String expected =
                ": you are going to place the following ships (which are all\n" +
                        "rectangular). For each ship, type the coordinate of the upper left\n" +
                        "side of the ship, followed by either H (for horizontal) or V (for\n" +
                        "vertical).  For example M4H would place a ship horizontally starting\n" +
                        "at M4 and going to the right.  You have\n" +
                        "\n" +
                        "2 \"Submarines\" ships that are 1x2\n" +
                        "3 \"Destroyers\" that are 1x3\n" +
                        "3 \"Battleships\" that are 1x4\n" +
                        "2 \"Carriers\" that are 1x6\n";
        assertEquals(expected, actual);
    }

    @Test
    void test_invalid_read_from_resources() {
        // invalid
        assertDoesNotThrow(() -> Utility.readFromResources("txt"));
        assertDoesNotThrow(() -> Utility.readFromResources(""));
    }

    @Test
    @ResourceLock(value = Resources.SYSTEM_OUT, mode = ResourceAccessMode.READ_WRITE)
    void test_readPlayerType() throws IOException {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        PrintStream out = new PrintStream(bytes, true);
        BufferedReader input = new BufferedReader(new StringReader("nnnnnn\n" +
                "COM\n" +
                "com\n" +
                "Human\n" +
                "HUMAN\n"));
        InputStream expectedStream = getClass().getClassLoader().getResourceAsStream("read_playerType_output.txt");
        assertNotNull(expectedStream);
        PrintStream oldOut = System.out;
        try {
            System.setOut(out);
            Utility.readPlayerType("test", input);
            Utility.readPlayerType("test", input);
            Utility.readPlayerType("test", input);
            Utility.readPlayerType("test", input);
        } finally {
            System.setOut(oldOut);
        }
        String expected = new String(expectedStream.readAllBytes());
        String actual = bytes.toString();
        assertEquals(expected, actual);
    }

    @Test
    void getRandomOrientation() {
        TreeSet<Character> actual = new TreeSet<>();
        TreeSet<Character> expected = new TreeSet<>() {
            {
                add('U');
                add('R');
                add('D');
                add('L');
            }
        };
        int counter = 0;
        while (counter++ < 1000) {
            actual.add(Utility.getRandomOrientation(false, randomGenerator));
        }
        assertDoesNotThrow(() -> Utility.getRandomOrientation(false, randomGenerator));
        assertEquals(expected, actual);
    }

    @Test
    void getRandomOrientation_rectangle() {
        TreeSet<Character> actual = new TreeSet<>();
        TreeSet<Character> expected = new TreeSet<>() {
            {
                add('V');
                add('H');
            }
        };
        int counter = 0;
        while (counter++ < 1000) {
            actual.add(Utility.getRandomOrientation(true, randomGenerator));
        }
        assertDoesNotThrow(() -> Utility.getRandomOrientation(true, randomGenerator));
        assertEquals(expected, actual);
    }

    @Test
    void getRandomNumber() {
        int range = 10;
        TreeSet<Integer> actual = new TreeSet<>();
        TreeSet<Integer> expected = new TreeSet<>() {
            {
                for (int i = 0; i < range; i++) {
                    add(i);
                }
            }
        };
        int counter = 0;
        while (counter++ < 1000) {
            actual.add(Utility.getRandomNumber(range, randomGenerator));
        }
        assertEquals(expected, actual);
    }
}