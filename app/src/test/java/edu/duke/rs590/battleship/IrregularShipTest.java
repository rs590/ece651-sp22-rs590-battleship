package edu.duke.rs590.battleship;

import org.junit.jupiter.api.Test;

import java.util.TreeMap;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class IrregularShipTest {

    @Test
    void makeBattleshipUpCoords() {
        TreeMap<Integer, Coordinate> expected = new TreeMap<>() {
            {
                put(1, new Coordinate(0, 1));
                put(2, new Coordinate(1, 0));
                put(3, new Coordinate(1, 1));
                put(4, new Coordinate(1, 2));
            }
        };
        TreeMap<Integer, Coordinate> actual = IrregularShip.makeBattleshipUpCoords(new Coordinate(0, 0));
        assertEquals(expected, actual);
    }

    @Test
    void makeBattleshipRightCoords() {
        TreeMap<Integer, Coordinate> expected = new TreeMap<>() {
            {
                put(2, new Coordinate(0, 0));
                put(3, new Coordinate(1, 0));
                put(4, new Coordinate(2, 0));
                put(1, new Coordinate(1, 1));
            }
        };
        TreeMap<Integer, Coordinate> actual = IrregularShip.makeBattleshipRightCoords(new Coordinate(0, 0));
        assertEquals(expected, actual);
    }

    @Test
    void makeBattleshipDownCoords() {
        TreeMap<Integer, Coordinate> expected = new TreeMap<>() {
            {
                put(2, new Coordinate(0, 0));
                put(3, new Coordinate(0, 1));
                put(4, new Coordinate(0, 2));
                put(1, new Coordinate(1, 1));
            }
        };
        TreeMap<Integer, Coordinate> actual = IrregularShip.makeBattleshipDownCoords(new Coordinate(0, 0));
        assertEquals(expected, actual);
    }

    @Test
    void makeBattleshipLeftCoords() {
        TreeMap<Integer, Coordinate> expected = new TreeMap<>() {
            {
                put(2, new Coordinate(0, 1));
                put(3, new Coordinate(1, 1));
                put(4, new Coordinate(2, 1));
                put(1, new Coordinate(1, 0));
            }
        };
        TreeMap<Integer, Coordinate> actual = IrregularShip.makeBattleshipLeftCoords(new Coordinate(0, 0));
        assertEquals(expected, actual);
    }

    @Test
    void makeCarrierUpCoords() {
        TreeMap<Integer, Coordinate> expected = new TreeMap<>() {
            {
                put(1, new Coordinate(0, 0));
                put(2, new Coordinate(1, 0));
                put(3, new Coordinate(2, 0));
                put(4, new Coordinate(3, 0));
                put(5, new Coordinate(2, 1));
                put(6, new Coordinate(3, 1));
                put(7, new Coordinate(4, 1));
            }
        };
        TreeMap<Integer, Coordinate> actual = IrregularShip.makeCarrierUpCoords(new Coordinate(0, 0));
        assertEquals(expected, actual);
    }

    @Test
    void makeCarrierRightCoords() {
        TreeMap<Integer, Coordinate> expected = new TreeMap<>() {
            {
                put(1, new Coordinate(0, 1));
                put(2, new Coordinate(0, 2));
                put(3, new Coordinate(0, 3));
                put(4, new Coordinate(0, 4));
                put(5, new Coordinate(1, 0));
                put(6, new Coordinate(1, 1));
                put(7, new Coordinate(1, 2));
            }
        };
        TreeMap<Integer, Coordinate> actual = IrregularShip.makeCarrierRightCoords(new Coordinate(0, 0));
        assertEquals(expected, actual);
    }

    @Test
    void makeCarrierDownCoords() {
        TreeMap<Integer, Coordinate> expected = new TreeMap<>() {
            {
                put(1, new Coordinate(1, 1));
                put(2, new Coordinate(2, 1));
                put(3, new Coordinate(3, 1));
                put(4, new Coordinate(4, 1));
                put(5, new Coordinate(0, 0));
                put(6, new Coordinate(1, 0));
                put(7, new Coordinate(2, 0));
            }
        };
        TreeMap<Integer, Coordinate> actual = IrregularShip.makeCarrierDownCoords(new Coordinate(0, 0));
        assertEquals(expected, actual);
    }

    @Test
    void makeCarrierLeftCoords() {
        TreeMap<Integer, Coordinate> expected = new TreeMap<>() {
            {
                put(1, new Coordinate(1, 0));
                put(2, new Coordinate(1, 1));
                put(3, new Coordinate(1, 2));
                put(4, new Coordinate(1, 3));
                put(5, new Coordinate(0, 2));
                put(6, new Coordinate(0, 3));
                put(7, new Coordinate(0, 4));
            }
        };
        TreeMap<Integer, Coordinate> actual = IrregularShip.makeCarrierLeftCoords(new Coordinate(0, 0));
        assertEquals(expected, actual);
    }

    @Test
    void test_invalid_name() {
        assertThrows(IllegalArgumentException.class, () -> IrregularShip.makeCoords("unknown", null));
    }
}