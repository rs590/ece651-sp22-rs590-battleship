package edu.duke.rs590.battleship;

import org.junit.jupiter.api.Test;

import java.io.*;
import java.util.ArrayList;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;

class RandomTextPlayerTest {

    public final Random randomGenerator = new Random(Constants.SEED);
    public final V2ShipFactory factory = new V2ShipFactory();
    public final BufferedReader input = new BufferedReader(new InputStreamReader(System.in));


    private RandomTextPlayer makeRandomTextPlayer() {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        PrintStream output = new PrintStream(bytes, true);
        Board<Character> b1 = new BattleShipBoard<>(10, 20, 'X');
        return new RandomTextPlayer("test", b1, input, output, factory, randomGenerator);
    }

    @Test
    void test_makeRandomPlacement() {
        RandomTextPlayer testPlayer = makeRandomTextPlayer();
        Placement expected = new Placement(new Coordinate(10, 0), 'H');
        Placement actual = testPlayer.makeRandomPlacement(true);
        assertEquals(expected, actual);
    }

    @Test
    void test_makeRandomCoordinate() {
        RandomTextPlayer testPlayer = makeRandomTextPlayer();
        Coordinate expected = new Coordinate(10, 0);
        Coordinate actual = testPlayer.makeRandomCoordinate();
        assertEquals(expected, actual);
    }

    @Test
    void doPlacementPhase() throws IOException {
        RandomTextPlayer testPlayer = makeRandomTextPlayer();
        testPlayer.doPlacementPhase();
        ArrayList<Ship<Character>> expected = new ArrayList<>() {
            {
                add(factory.makeSubmarine(new Placement("K0H")));
                add(factory.makeSubmarine(new Placement("E6H")));
                add(factory.makeDestroyer(new Placement("A2V")));
                add(factory.makeDestroyer(new Placement("L1H")));
                add(factory.makeDestroyer(new Placement("G6H")));
                add(factory.makeBattleship(new Placement("P5R")));
                add(factory.makeBattleship(new Placement("A0R")));
                add(factory.makeBattleship(new Placement("M6R")));
                add(factory.makeCarrier(new Placement("R0L")));
                add(factory.makeCarrier(new Placement("A3R")));
            }
        };

        assertEquals(expected, testPlayer.theBoard.getMyShips());
    }

    @Test
    void playOneTurn() {
        RandomTextPlayer testPlayer = makeRandomTextPlayer();
        RandomTextPlayer testEnemy = makeRandomTextPlayer();
        testPlayer.doPlacementPhase();
        testEnemy.doPlacementPhase();
        int counter = 0;
        while (counter++ < 1000) {
            testPlayer.playOneTurn(testEnemy);
            testEnemy.playOneTurn(testPlayer);
        }
    }
}