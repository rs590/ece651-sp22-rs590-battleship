package edu.duke.rs590.battleship;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PlacementTest {
    @Test
    void test_string_constructor_valid_cases() {
        Placement p1 = new Placement("B3H");
        assertEquals(new Coordinate("B3"), p1.getCoordinate());
        assertEquals('H', p1.getOrientation());
        Placement p2 = new Placement("D5V");
        assertEquals(new Coordinate("D5"), p2.getCoordinate());
        assertEquals('V', p2.getOrientation());
        Placement p3 = new Placement("A9H");
        assertEquals(new Coordinate("A9"), p3.getCoordinate());
        assertEquals('H', p3.getOrientation());
        Placement p4 = new Placement("Z0V");
        assertEquals(new Coordinate("Z0"), p4.getCoordinate());
        assertEquals('V', p4.getOrientation());
    }

    @Test
    public void test_string_constructor_error_cases() {
        assertThrows(IllegalArgumentException.class, () -> new Placement("000"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("AAA"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("@0A"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("[0/"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("A/1"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("A:]"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("A"));
    }

    @Test
    public void test_equals() {
        Coordinate c1 = new Coordinate(2, 1);
        Placement p1 = new Placement(c1, 'V');
        Placement p2 = new Placement(c1, 'v');
        Placement p3 = new Placement("C1V");
        Placement p4 = new Placement("C1V");
        Placement p5 = new Placement("C2V");
        assertEquals(p1, p2);
        assertEquals(p1, p3);
        assertEquals(p3, p4);
        assertNotEquals(p1, p5);
        assertNotEquals(p1, "(2,1), V");
    }

    @Test
    public void test_hashCode() {
        Placement p1 = new Placement("C1V");
        Placement p2 = new Placement("C1V");
        Placement p3 = new Placement("C2V");
        Placement p4 = new Placement("Z0H");
        assertEquals(p1.hashCode(), p2.hashCode());
        assertNotEquals(p1.hashCode(), p3.hashCode());
        assertNotEquals(p1.hashCode(), p4.hashCode());
    }
}