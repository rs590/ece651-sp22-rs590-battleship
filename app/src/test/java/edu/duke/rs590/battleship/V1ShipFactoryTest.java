package edu.duke.rs590.battleship;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class V1ShipFactoryTest {
    V1ShipFactory factory = new V1ShipFactory();

    void checkShip(Ship<Character> testShip, String expectedName,
                   char expectedLetter, Coordinate... expectedLocs) {
        assertEquals(expectedName, testShip.getName());
        for (Coordinate c : expectedLocs) {
            assertEquals(expectedLetter, testShip.getDisplayInfoAt(c, true));
            assertTrue(testShip.occupiesCoordinates(c));
        }
    }

    @Test
    void test_makeSubmarine() {
        Placement v1_2_V = new Placement(new Coordinate(1, 2), 'V');
        Ship<Character> sub_V = factory.makeSubmarine(v1_2_V);
        checkShip(sub_V, "Submarine", 's',
                new Coordinate(1, 2),
                new Coordinate(2, 2));
        Placement v1_2_H = new Placement(new Coordinate(1, 2), 'H');
        Ship<Character> sub_H = factory.makeSubmarine(v1_2_H);
        checkShip(sub_H, "Submarine", 's',
                new Coordinate(1, 2),
                new Coordinate(1, 3));
    }

    @Test
    void test_makeSubmarine_invalid() {
        Placement v1_2 = new Placement(new Coordinate(1, 2), '/');
        assertThrows(IllegalArgumentException.class, () -> factory.makeSubmarine(v1_2));
    }

    @Test
    void test_makeBattleship() {
        Placement v1_2_V = new Placement(new Coordinate(1, 2), 'V');
        Ship<Character> bat_V = factory.makeBattleship(v1_2_V);
        checkShip(bat_V, "Battleship", 'b',
                new Coordinate(1, 2),
                new Coordinate(2, 2),
                new Coordinate(3, 2),
                new Coordinate(4, 2));
        Placement v1_2_H = new Placement(new Coordinate(1, 2), 'H');
        Ship<Character> bat_H = factory.makeBattleship(v1_2_H);
        checkShip(bat_H, "Battleship", 'b',
                new Coordinate(1, 2),
                new Coordinate(1, 3),
                new Coordinate(1, 4),
                new Coordinate(1, 5));
    }

    @Test
    void test_makeBattleship_invalid() {
        Placement v1_2 = new Placement(new Coordinate(1, 2), '/');
        assertThrows(IllegalArgumentException.class, () -> factory.makeBattleship(v1_2));
    }

    @Test
    void test_makeCarrier() {
        Placement v1_2_V = new Placement(new Coordinate(1, 2), 'V');
        Ship<Character> car_V = factory.makeCarrier(v1_2_V);
        checkShip(car_V, "Carrier", 'c',
                new Coordinate(1, 2),
                new Coordinate(2, 2),
                new Coordinate(3, 2),
                new Coordinate(4, 2),
                new Coordinate(5, 2),
                new Coordinate(6, 2));
        Placement v1_2_H = new Placement(new Coordinate(1, 2), 'H');
        Ship<Character> car_H = factory.makeCarrier(v1_2_H);
        checkShip(car_H, "Carrier", 'c',
                new Coordinate(1, 2),
                new Coordinate(1, 3),
                new Coordinate(1, 4),
                new Coordinate(1, 5),
                new Coordinate(1, 6),
                new Coordinate(1, 7));
    }

    @Test
    void test_makeCarrier_invalid() {
        Placement v1_2 = new Placement(new Coordinate(1, 2), '/');
        assertThrows(IllegalArgumentException.class, () -> factory.makeCarrier(v1_2));
    }

    @Test
    void test_makeDestroyer() {
        Placement v1_2_V = new Placement(new Coordinate(1, 2), 'V');
        Ship<Character> des_V = factory.makeDestroyer(v1_2_V);
        checkShip(des_V, "Destroyer", 'd',
                new Coordinate(1, 2),
                new Coordinate(2, 2),
                new Coordinate(3, 2));
        Placement v1_2_H = new Placement(new Coordinate(1, 2), 'H');
        Ship<Character> des_H = factory.makeDestroyer(v1_2_H);
        checkShip(des_H, "Destroyer", 'd',
                new Coordinate(1, 2),
                new Coordinate(1, 3),
                new Coordinate(1, 4));
    }

    @Test
    void test_makeDestroyer_invalid() {
        Placement v1_2 = new Placement(new Coordinate(1, 2), '/');
        assertThrows(IllegalArgumentException.class, () -> factory.makeDestroyer(v1_2));
    }
}