package edu.duke.rs590.battleship;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

class NoCollisionRuleCheckerTest {
    V1ShipFactory factory = new V1ShipFactory();

    @Test
    public void test_no_collision_rule() {
        NoCollisionRuleChecker<Character> checker = new NoCollisionRuleChecker<>(null);
        Board<Character> board = new BattleShipBoard<>(10, 20, 'X', checker);

        // invalid
        Placement p_H = new Placement(new Coordinate(6, 6), 'H');
        Placement p_V = new Placement(new Coordinate(6, 6), 'V');
        Ship<Character> ship_H = factory.makeSubmarine(p_H);
        Ship<Character> ship_V = factory.makeCarrier(p_V);
        // The placement of ship_H is valid
        assertNull(checker.checkPlacement(ship_H, board));
        board.tryAddShip(ship_H);
        // The placement of ship_V is invalid, collate with ship_H
        assertNotNull(checker.checkPlacement(ship_V, board));
        board.tryAddShip(ship_V);

        // valid
        Placement p_valid = new Placement(new Coordinate(0, 0), 'V');
        Placement p_valid2 = new Placement(new Coordinate(0, 1), 'H');
        Ship<Character> ship_valid = factory.makeSubmarine(p_valid);
        Ship<Character> ship_valid2 = factory.makeSubmarine(p_valid2);
        // The placement of ship_valid is valid
        assertNull(checker.checkPlacement(ship_valid, board));
        board.tryAddShip(ship_valid);
        // The placement of ship_valid2 is valid
        assertNull(checker.checkPlacement(ship_valid2, board));
        board.tryAddShip(ship_valid2);
    }

    @Test
    public void test_multi_rules() {
        InBoundsRuleChecker<Character> checker = new InBoundsRuleChecker<>(new NoCollisionRuleChecker<>(null));
        Board<Character> board = new BattleShipBoard<>(10, 20, 'X', checker);
        // valid placement
        Placement p_0 = new Placement(new Coordinate(0, 0), 'H');
        Placement p_1 = new Placement(new Coordinate(1, 0), 'H');
        Placement p_2 = new Placement(new Coordinate(2, 0), 'H');
        // valid ships
        Ship<Character> ship_p0 = factory.makeSubmarine(p_0);
        assertNull(checker.checkPlacement(ship_p0, board));
        board.tryAddShip(ship_p0);

        Ship<Character> ship_p1 = factory.makeSubmarine(p_1);
        assertNull(checker.checkPlacement(ship_p1, board));
        board.tryAddShip(ship_p1);

        Ship<Character> ship_p2 = factory.makeSubmarine(p_2);
        assertNull(checker.checkPlacement(ship_p2, board));
        board.tryAddShip(ship_p2);

        // invalid placement & ships
        // same with p_0
        Placement p_3 = new Placement(new Coordinate(0, 0), 'H');
        // ship with this placement may collate with p_1
        Placement p_4 = new Placement(new Coordinate(1, 1), 'V');
        // placement out of bound
        Placement p_5 = new Placement(new Coordinate(-1, -1), 'H');

        Ship<Character> ship_p3 = factory.makeSubmarine(p_3);
        assertNotNull(checker.checkPlacement(ship_p3, board));

        Ship<Character> ship_p4 = factory.makeSubmarine(p_4);
        assertNotNull(checker.checkPlacement(ship_p4, board));

        Ship<Character> ship_p5 = factory.makeSubmarine(p_5);
        assertNotNull(checker.checkPlacement(ship_p5, board));

    }

}