package edu.duke.rs590.battleship;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ConstantsTest {
    @Test
    void test_constant() {
        Constants constants = new Constants();
        String headerFooter = "--------------------------------------------------------------------------------\n";
        String prompt_placement =
                ": you are going to place the following ships (which are all\n" +
                        "rectangular). For each ship, type the coordinate of the upper left\n" +
                        "side of the ship, followed by either H (for horizontal) or V (for\n" +
                        "vertical).  For example M4H would place a ship horizontally starting\n" +
                        "at M4 and going to the right.  You have\n" +
                        "\n" +
                        "2 \"Submarines\" ships that are 1x2\n" +
                        "3 \"Destroyers\" that are 1x3\n" +
                        "3 \"Battleships\" that are 1x4\n" +
                        "2 \"Carriers\" that are 1x6\n";
        assertEquals(headerFooter, constants.HEADER_FOOTER);
        assertEquals(prompt_placement, constants.PROMPT_PLACEMENT);
    }

}