package edu.duke.rs590.battleship;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.parallel.ResourceAccessMode;
import org.junit.jupiter.api.parallel.ResourceLock;
import org.junit.jupiter.api.parallel.Resources;

import java.io.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class V2TextPlayerTest {

    private V2TextPlayer createTextPlayer(int w, int h, String inputData, OutputStream bytes) {
        BufferedReader input = new BufferedReader(new StringReader(inputData));
        PrintStream output = new PrintStream(bytes, true);
        Board<Character> board = new BattleShipBoard<>(w, h, 'X');
        V2ShipFactory shipFactory = new V2ShipFactory();
        return new V2TextPlayer("A", board, input, output, shipFactory);
    }

    @Test
    void test_do_one_placement() throws IOException {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        TextPlayer player = createTextPlayer(4, 3, "B2M\nB2V\n", bytes);
        String prompt = "Player A where do you want to place a Submarine?";
        String expected = "--------------------------------------------------------------------------------\n" +
                "Player A where do you want to place a Submarine?\n" +
                "The orientation must be 'V'('v') or 'H'('h')but is M\n" +
                "Please try again.\n" +
                "--------------------------------------------------------------------------------\n" +
                "Player A where do you want to place a Submarine?\n" +
                "--------------------------------------------------------------------------------\n" +
                "  0|1|2|3\n" +
                "A  | | |  A\n" +
                "B  | |s|  B\n" +
                "C  | |s|  C\n" +
                "  0|1|2|3\n";
        player.doOnePlacement("Submarine", player.shipCreationFns.get("Submarine"));
        assertEquals(expected, bytes.toString());
    }

    @Test
    @ResourceLock(value = Resources.SYSTEM_OUT, mode = ResourceAccessMode.READ_WRITE)
    void makeChoice_s() throws IOException {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        PrintStream out = new PrintStream(bytes, true);
        InputStream expectedStream = getClass().getClassLoader().getResourceAsStream("action_s.txt");
        assertNotNull(expectedStream);
        V2TextPlayer player = createTextPlayer(10, 20, "s\na0\ns\na0\ns\na0\ns\nf\na0\n", bytes);
        V2TextPlayer enemy = createTextPlayer(10, 20, "s\na0\n", bytes);
        PrintStream oldOut = System.out;
        try {
            System.setOut(out);
            player.makeChoice(enemy);
            player.makeChoice(enemy);
            player.makeChoice(enemy);
            player.makeChoice(enemy);
        } finally {
            System.setOut(oldOut);
        }
        String expected = new String(expectedStream.readAllBytes());
        String actual = bytes.toString();
        assertEquals(expected, actual);
    }

    @Test
    @ResourceLock(value = Resources.SYSTEM_OUT, mode = ResourceAccessMode.READ_WRITE)
    void makeChoice_f() throws IOException {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        PrintStream out = new PrintStream(bytes, true);
        InputStream expectedStream = getClass().getClassLoader().getResourceAsStream("action_f.txt");
        assertNotNull(expectedStream);
        V2TextPlayer player = createTextPlayer(10, 20, "f\na0\nf\nc0\n", bytes);
        V2TextPlayer enemy = createTextPlayer(10, 20, "\n", bytes);
        enemy.theBoard.tryAddShip(player.shipFactory.makeSubmarine(new Placement("A0H")));
        PrintStream oldOut = System.out;
        try {
            System.setOut(out);
            player.makeChoice(enemy);
            player.makeChoice(enemy);
        } catch (EOFException e) {

        } finally {
            System.setOut(oldOut);
        }
        String expected = new String(expectedStream.readAllBytes());
        String actual = bytes.toString();
        assertEquals(expected, actual);
    }

    @Test
    @ResourceLock(value = Resources.SYSTEM_OUT, mode = ResourceAccessMode.READ_WRITE)
    void makeChoice_m() throws IOException {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        PrintStream out = new PrintStream(bytes, true);
        InputStream expectedStream = getClass().getClassLoader().getResourceAsStream("action_m.txt");
        assertNotNull(expectedStream);
        V2TextPlayer player = createTextPlayer(10, 20, "m\na0\na0v\nm\na0\na0h\nm\na0\na0v\nm\nf\na0\n", bytes);
        player.theBoard.tryAddShip(player.shipFactory.makeSubmarine(new Placement("A0H")));
        V2TextPlayer enemy = createTextPlayer(10, 20, "s\na0\n", bytes);
        PrintStream oldOut = System.out;
        try {
            System.setOut(out);
            player.makeChoice(enemy);
            player.makeChoice(enemy);
            player.makeChoice(enemy);
            player.makeChoice(enemy);
        } catch (EOFException e) {

        } finally {
            System.setOut(oldOut);
        }
        String expected = new String(expectedStream.readAllBytes());
        String actual = bytes.toString();
        assertEquals(expected, actual);
    }

    @Test
    @ResourceLock(value = Resources.SYSTEM_OUT, mode = ResourceAccessMode.READ_WRITE)
    void makeChoice_invalid() throws IOException {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        PrintStream out = new PrintStream(bytes, true);
        InputStream expectedStream = getClass().getClassLoader().getResourceAsStream("invalid_choice.txt");
        assertNotNull(expectedStream);
        V2TextPlayer player = createTextPlayer(10, 20, "z\nf\na0\n", bytes);
        player.theBoard.tryAddShip(player.shipFactory.makeSubmarine(new Placement("A0H")));
        V2TextPlayer enemy = createTextPlayer(10, 20, "s\na0\n", bytes);
        PrintStream oldOut = System.out;
        try {
            System.setOut(out);
            player.makeChoice(enemy);
            player.makeChoice(enemy);
            player.makeChoice(enemy);
            player.makeChoice(enemy);
        } catch (EOFException e) {

        } finally {
            System.setOut(oldOut);
        }
        String expected = new String(expectedStream.readAllBytes());
        String actual = bytes.toString();
        assertEquals(expected, actual);
    }

    @Test
    void moveAction_valid() throws IOException {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        PrintStream out = new PrintStream(bytes, true);
        InputStream expectedStream = getClass().getClassLoader().getResourceAsStream(Constants.TEST_MOVE_OUTPUT);
        assertNotNull(expectedStream);
        V2TextPlayer player = createTextPlayer(10, 20, "a0\nm\nb0\nb0v\n", bytes);
        Ship<Character> sub = player.shipFactory.makeSubmarine(new Placement(new Coordinate(0, 0), 'h'));
        Ship<Character> des = player.shipFactory.makeDestroyer(new Placement(new Coordinate(1, 0), 'h'));
        player.theBoard.tryAddShip(sub);
        player.theBoard.tryAddShip(des);
        sub.recordHitAt(new Coordinate(0, 0));
        sub.recordHitAt(new Coordinate(0, 1));
        PrintStream oldOut = System.out;
        try {
            System.setOut(out);
            player.moveAction(null);
        } finally {
            System.setOut(oldOut);
        }
        String expected = new String(expectedStream.readAllBytes());
        String actual = bytes.toString();
        assertEquals(expected, actual);
    }

    @Test
    void moveAction_invalid() throws IOException {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        PrintStream out = new PrintStream(bytes, true);
        InputStream expectedStream = getClass().getClassLoader().getResourceAsStream("invalid_move_output.txt");
        assertNotNull(expectedStream);
        V2TextPlayer player = createTextPlayer(10, 20, "b0\na0v\nm\nb0\nc0h\n", bytes);
        Ship<Character> sub = player.shipFactory.makeSubmarine(new Placement(new Coordinate(0, 0), 'h'));
        Ship<Character> des = player.shipFactory.makeDestroyer(new Placement(new Coordinate(1, 0), 'h'));
        player.theBoard.tryAddShip(sub);
        player.theBoard.tryAddShip(des);
        sub.recordHitAt(new Coordinate(0, 0));
        sub.recordHitAt(new Coordinate(0, 1));
        PrintStream oldOut = System.out;
        try {
            System.setOut(out);
            player.moveAction(null);
        } finally {
            System.setOut(oldOut);
        }
        String expected = new String(expectedStream.readAllBytes());
        String actual = bytes.toString();
        assertEquals(expected, actual);
    }

    @Test
    void sonarAction() throws IOException {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        PrintStream out = new PrintStream(bytes, true);
        InputStream expectedStream = getClass().getClassLoader().getResourceAsStream(Constants.TEST_SONAR_OUTPUT);
        assertNotNull(expectedStream);
        V2TextPlayer player = createTextPlayer(10, 20, "a0\na9\nt0\n", bytes);
        V2TextPlayer enemy = createTextPlayer(10, 20, "null", bytes);
        Ship<Character> sub = player.shipFactory.makeSubmarine(new Placement(new Coordinate(0, 0), 'h'));
        Ship<Character> des = player.shipFactory.makeDestroyer(new Placement(new Coordinate(1, 0), 'h'));
        enemy.theBoard.tryAddShip(sub);
        enemy.theBoard.tryAddShip(des);
        sub.recordHitAt(new Coordinate(0, 0));
        sub.recordHitAt(new Coordinate(0, 1));
        PrintStream oldOut = System.out;
        try {
            System.setOut(out);
            player.sonarAction(enemy);
            player.sonarAction(enemy);
            player.sonarAction(enemy);
        } finally {
            System.setOut(oldOut);
        }
        String expected = new String(expectedStream.readAllBytes());
        String actual = bytes.toString();
        assertEquals(expected, actual);
    }

    @Test
    @ResourceLock(value = Resources.SYSTEM_OUT, mode = ResourceAccessMode.READ_WRITE)
    void test_do_placement_phase() throws IOException {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        PrintStream out = new PrintStream(bytes, true);
        String input_str = Utility.readFromResources("placement_input_v2.txt");
        InputStream input = getClass().getClassLoader().getResourceAsStream("placement_input_v2.txt");
        assertNotNull(input);
        InputStream expectedStream = getClass().getClassLoader().getResourceAsStream("placement_output_v2.txt");
        assertNotNull(expectedStream);
        InputStream oldIn = System.in;
        PrintStream oldOut = System.out;
        try {
            System.setIn(input);
            System.setOut(out);
            V2TextPlayer player = createTextPlayer(10, 20, input_str, bytes);
            player.doPlacementPhase();
        } finally {
            System.setIn(oldIn);
            System.setOut(oldOut);
        }
        String expected = new String(expectedStream.readAllBytes());
        String actual = bytes.toString();
        assertEquals(expected, actual);
    }

    @Test
    @ResourceLock(value = Resources.SYSTEM_OUT, mode = ResourceAccessMode.READ_WRITE)
    void test_play_one_turn() throws IOException {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        PrintStream out = new PrintStream(bytes, true);
        InputStream expectedStream = getClass().getClassLoader().getResourceAsStream("play_one_turn_v2.txt");
        assertNotNull(expectedStream);
        String input_str = "m\na0\na0v\nm\na0\na0h\nm\na0\na0v\n" +
                "s\na0\ns\na0\ns\na0\n" + "a0\n";
        V2TextPlayer player = createTextPlayer(10, 20, input_str, bytes);
        V2TextPlayer enemy = createTextPlayer(10, 20, "null", bytes);
        Ship<Character> sub = player.shipFactory.makeSubmarine(new Placement(new Coordinate(0, 0), 'h'));
        Ship<Character> des = player.shipFactory.makeDestroyer(new Placement(new Coordinate(7, 0), 'h'));
        player.theBoard.tryAddShip(sub);
        player.theBoard.tryAddShip(des);
        PrintStream oldOut = System.out;
        try {
            System.setOut(out);
            player.playOneTurn(enemy);
            player.playOneTurn(enemy);
            player.playOneTurn(enemy);
            player.playOneTurn(enemy);
            player.playOneTurn(enemy);
            player.playOneTurn(enemy);
            player.playOneTurn(enemy);
        } finally {
            System.setOut(oldOut);
        }
        String expected = new String(expectedStream.readAllBytes());
        String actual = bytes.toString();
        assertEquals(expected, actual);
    }
}