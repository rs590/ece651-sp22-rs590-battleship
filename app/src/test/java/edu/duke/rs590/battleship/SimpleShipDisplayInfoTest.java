package edu.duke.rs590.battleship;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SimpleShipDisplayInfoTest {
    @Test
    public void test_get_info() {
        SimpleShipDisplayInfo<Character> info = new SimpleShipDisplayInfo<>('s', '*');
        assertEquals('s', info.getInfo(null, false));
        assertEquals('*', info.getInfo(null, true));
    }

}