package edu.duke.rs590.battleship;

import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class BoardTextViewTest {
    private void emptyBoardHelper(int w, int h, String expectedHeader, String expectedBody){
        Board<Character> b1 = new BattleShipBoard<>(w, h, 'X');
        BoardTextView view = new BoardTextView(b1);
        assertEquals(expectedHeader, view.makeHeader());
        String expected = expectedHeader + expectedBody + expectedHeader;
        assertEquals(expected, view.displayMyOwnBoard());
    }

    @Test
    public void test_invalid_board_size() {
        Board<Character> wideBoard = new BattleShipBoard<>(11, 20, 'X');
        Board<Character> tallBoard = new BattleShipBoard<>(10, 27, 'X');
        assertThrows(IllegalArgumentException.class, () -> new BoardTextView(wideBoard));
        assertThrows(IllegalArgumentException.class, () -> new BoardTextView(tallBoard));
    }

    @Test
    public void test_display_empty_2by2() {
        int width = 2, height = 2;
        String expectedHeader= "  0|1\n";
        String expectedBody =
                "A  |  A\n" +
                "B  |  B\n";
        emptyBoardHelper(width, height, expectedHeader, expectedBody);
    }

    @Test
    public void test_display_empty_3by2() {
        int width = 3, height = 2;
        String expectedHeader = "  0|1|2\n";
        String expectedBody =
                "A  | |  A\n" +
                "B  | |  B\n";
        emptyBoardHelper(width, height, expectedHeader, expectedBody);
    }

    @Test
    public void test_display_empty_3by5() {
        int width = 3, height = 5;
        String expectedHeader = "  0|1|2\n";
        String expectedBody =
                "A  | |  A\n" +
                "B  | |  B\n" +
                "C  | |  C\n" +
                "D  | |  D\n" +
                "E  | |  E\n";
        emptyBoardHelper(width, height, expectedHeader, expectedBody);
    }

    @Test
    public void test_display_4by3() {
        int width = 4, height = 3;
        Board<Character> b1 = new BattleShipBoard<>(width, height, 'X');
        BoardTextView view = new BoardTextView(b1);
        b1.tryAddShip(new RectangleShip<>(new Coordinate(0, 1), 's', '*'));
        b1.tryAddShip(new RectangleShip<>(new Coordinate(1, 2), 's', '*'));
        b1.tryAddShip(new RectangleShip<>(new Coordinate(2, 3), 's', '*'));
        String expectedHeader = "  0|1|2|3\n";
        String expectedBody =
                "A  |s| |  A\n" +
                        "B  | |s|  B\n" +
                        "C  | | |s C\n";
        String expected = expectedHeader + expectedBody + expectedHeader;
        assertEquals(expected, view.displayMyOwnBoard());
    }

    @Test
    public void test_display_enemy_board() {
        int width = 4, height = 3;
        Board<Character> b1 = new BattleShipBoard<>(width, height, 'X');
        BoardTextView view = new BoardTextView(b1);
        V1ShipFactory factory = new V1ShipFactory();
        Ship<Character> sub = factory.makeSubmarine(new Placement(new Coordinate(1, 0), 'H'));
        Ship<Character> des = factory.makeDestroyer(new Placement(new Coordinate(0, 3), 'V'));
        b1.tryAddShip(sub);
        b1.tryAddShip(des);
        String myView =
                "  0|1|2|3\n" +
                        "A  | | |d A\n" +
                        "B s|s| |d B\n" +
                        "C  | | |d C\n" +
                        "  0|1|2|3\n";
        // self view
        assertEquals(myView, view.displayMyOwnBoard());

        // enemy view
        // hit ship
        b1.fireAt(new Coordinate(1, 0));
        String hit_enemy_view =
                "  0|1|2|3\n" +
                        "A  | | |  A\n" +
                        "B s| | |  B\n" +
                        "C  | | |  C\n" +
                        "  0|1|2|3\n";
        String hit_my_view =
                "  0|1|2|3\n" +
                        "A  | | |d A\n" +
                        "B *|s| |d B\n" +
                        "C  | | |d C\n" +
                        "  0|1|2|3\n";
        assertEquals(hit_enemy_view, view.displayEnemyBoard());
        assertEquals(hit_my_view, view.displayMyOwnBoard());
        // miss hit
        b1.fireAt(new Coordinate(2, 0));
        String miss_enemy_view =
                "  0|1|2|3\n" +
                        "A  | | |  A\n" +
                        "B s| | |  B\n" +
                        "C X| | |  C\n" +
                        "  0|1|2|3\n";
        String miss_my_view =
                "  0|1|2|3\n" +
                        "A  | | |d A\n" +
                        "B *|s| |d B\n" +
                        "C  | | |d C\n" +
                        "  0|1|2|3\n";
        assertEquals(miss_enemy_view, view.displayEnemyBoard());
        assertEquals(miss_my_view, view.displayMyOwnBoard());
    }

    @Test
    public void test_two_view() throws IOException {
        int width = 10, height = 20;
        Board<Character> myBoard = new BattleShipBoard<>(width, height, 'X');
        BoardTextView myView = new BoardTextView(myBoard);
        Board<Character> enemyBoard = new BattleShipBoard<>(width, height, 'X');
        BoardTextView enemyView = new BoardTextView(enemyBoard);
        String enemyHeader = "Player B's ocean";

        assertEquals(Utility.readFromResources("expected_two_view.txt"),
                myView.displayMyBoardWithEnemyNextToIt(enemyView, Constants.MY_HEADER, enemyHeader));
    }
}