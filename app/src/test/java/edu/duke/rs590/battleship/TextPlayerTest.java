package edu.duke.rs590.battleship;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.parallel.ResourceAccessMode;
import org.junit.jupiter.api.parallel.ResourceLock;
import org.junit.jupiter.api.parallel.Resources;

import java.io.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class TextPlayerTest {

    private TextPlayer createTextPlayer(int w, int h, String inputData, OutputStream bytes) {
        BufferedReader input = new BufferedReader(new StringReader(inputData));
        PrintStream output = new PrintStream(bytes, true);
        Board<Character> board = new BattleShipBoard<>(w, h, 'X');
        V1ShipFactory shipFactory = new V1ShipFactory();
        return new TextPlayer("A", board, input, output, shipFactory);
    }

    @Test
    void test_read_placement() throws IOException {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        TextPlayer player = createTextPlayer(10, 20, "B2V\nC8H\na4v\n", bytes);
        String prompt = "Please enter a location for a ship:";
        Placement[] expected = new Placement[3];
        expected[0] = new Placement(new Coordinate(1, 2), 'V');
        expected[1] = new Placement(new Coordinate(2, 8), 'H');
        expected[2] = new Placement(new Coordinate(0, 4), 'V');
        for (Placement placement : expected) {
            Placement p = player.readPlacement(prompt);
            assertEquals(p, placement); //did we get the right Placement back
            assertEquals(prompt + "\n", bytes.toString()); //should have printed prompt and newline
            bytes.reset(); //clear out bytes for next time around
        }
    }

    @Test
    void test_read_coordinate() throws IOException {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        TextPlayer player = createTextPlayer(10, 20, "11\n111\nz9\nA0", bytes);
        String prompt = Utility.readFromResources("read_coor_output.txt");
        Coordinate expected = new Coordinate(0, 0);
        Coordinate c = player.readCoordinate();
        assertEquals(expected, c); //did we get the right Placement back
        assertEquals(prompt, bytes.toString()); //should have printed prompt and newline
        bytes.reset(); //clear out bytes for next time around


    }

    @Test
    void test_do_one_placement() throws IOException {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        TextPlayer player = createTextPlayer(4, 3, "B2V\n", bytes);
        String prompt = "Player A where do you want to place a Submarine?";
        String expected =
                Constants.HEADER_FOOTER +
                        prompt + "\n" +
                        Constants.HEADER_FOOTER +
                        "  0|1|2|3\n" +
                        "A  | | |  A\n" +
                        "B  | |s|  B\n" +
                        "C  | |s|  C\n" +
                        "  0|1|2|3\n";
        player.doOnePlacement("Submarine", player.shipCreationFns.get("Submarine"));
        assertEquals(expected, bytes.toString());
    }

    @Test
    @ResourceLock(value = Resources.SYSTEM_OUT, mode = ResourceAccessMode.READ_WRITE)
    void test_do_placement_phase() throws IOException {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        PrintStream out = new PrintStream(bytes, true);
        String input_str = Utility.readFromResources(Constants.TEST_PLACEMENT_INPUT);
        InputStream input = getClass().getClassLoader().getResourceAsStream(Constants.TEST_PLACEMENT_INPUT);
        assertNotNull(input);
        InputStream expectedStream = getClass().getClassLoader().getResourceAsStream(Constants.TEST_PLACEMENT_OUTPUT);
        assertNotNull(expectedStream);
        TextPlayer player = createTextPlayer(10, 20, input_str, bytes);
        InputStream oldIn = System.in;
        PrintStream oldOut = System.out;
        try {
            System.setIn(input);
            System.setOut(out);
            player.doPlacementPhase();
        } finally {
            System.setIn(oldIn);
            System.setOut(oldOut);
        }
        String expected = new String(expectedStream.readAllBytes());
        String actual = bytes.toString();
        assertEquals(expected, actual);
    }

    @Test
    @ResourceLock(value = Resources.SYSTEM_OUT, mode = ResourceAccessMode.READ_WRITE)
    void test_do_placement_phase_invalid_placement() throws IOException {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        PrintStream out = new PrintStream(bytes, true);
        String input_str = Utility.readFromResources(Constants.TEST_INVALID_PLACEMENT_INPUT);
        InputStream input = getClass().getClassLoader().getResourceAsStream(Constants.TEST_INVALID_PLACEMENT_INPUT);
        assertNotNull(input);
        InputStream expectedStream = getClass().getClassLoader().getResourceAsStream(Constants.TEST_INVALID_PLACEMENT_OUTPUT);
        assertNotNull(expectedStream);
        TextPlayer player = createTextPlayer(10, 20, input_str, bytes);
        InputStream oldIn = System.in;
        PrintStream oldOut = System.out;
        try {
            System.setIn(input);
            System.setOut(out);
            player.doPlacementPhase();
        } finally {
            System.setIn(oldIn);
            System.setOut(oldOut);
        }
        String expected = new String(expectedStream.readAllBytes());
        String actual = bytes.toString();
        assertEquals(expected, actual);
    }

}