package edu.duke.rs590.battleship;

import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeMap;

import static edu.duke.rs590.battleship.RectangleShip.makeCoords;
import static org.junit.jupiter.api.Assertions.*;

class RectangleShipTest {

    @Test
    public void test_makeCoords() {
        TreeMap<Integer, Coordinate> expected = new TreeMap<>();
        expected.put(1, new Coordinate(1, 2));
        expected.put(2, new Coordinate(2, 2));
        expected.put(3, new Coordinate(3, 2));
        Coordinate upperLeft = new Coordinate(1, 2);
        int width = 1, height = 3;
        TreeMap<Integer, Coordinate> actual = makeCoords(upperLeft, width, height);
        assertEquals(expected, actual);
    }

    @Test
    public void test_rectangle_ship() {
        HashMap<Coordinate, Boolean> expected = new HashMap<>();
        expected.put(new Coordinate(1, 2), false);
        expected.put(new Coordinate(2, 2), false);
        expected.put(new Coordinate(3, 2), false);
        Coordinate upperLeft = new Coordinate(1, 2);
        int width = 1, height = 3;
        RectangleShip<Character> rectangleShip = new RectangleShip<>(upperLeft, width, height, 's', '*');
        HashMap<Coordinate, Boolean> actual = rectangleShip.myPieces;
        assertEquals(expected, actual);
        assertEquals("test-ship", rectangleShip.getName());
    }

    @Test
    public void test_record_hit_at() {
        HashMap<Coordinate, Boolean> expected = new HashMap<>();
        expected.put(new Coordinate(1, 2), true);
        expected.put(new Coordinate(2, 2), false);
        expected.put(new Coordinate(3, 2), true);
        Coordinate upperLeft = new Coordinate(1, 2);
        int width = 1, height = 3;
        RectangleShip<Character> r = new RectangleShip<>(upperLeft, width, height, 's', '*');
        HashMap<Coordinate, Boolean> actual = r.myPieces;
        r.recordHitAt(new Coordinate(1, 2));
        r.recordHitAt(new Coordinate(3, 2));
        assertEquals("test-ship", r.getName());
        assertEquals(expected, actual);
        assertThrows(IllegalArgumentException.class, () -> r.recordHitAt(new Coordinate(4, 2)));
    }

    @Test
    public void test_was_hit_at() {
        Coordinate upperLeft = new Coordinate(1, 2);
        int width = 1, height = 3;
        RectangleShip<Character> r = new RectangleShip<>(upperLeft, width, height, 's', '*');
        r.recordHitAt(new Coordinate(1, 2));
        r.recordHitAt(new Coordinate(3, 2));
        assertEquals("test-ship", r.getName());
        assertTrue(r.wasHitAt(new Coordinate(1, 2)));
        assertTrue(r.wasHitAt(new Coordinate(3, 2)));
        assertFalse(r.wasHitAt(new Coordinate(2, 2)));
        assertThrows(IllegalArgumentException.class, () -> r.wasHitAt(new Coordinate(4, 2)));
    }

    @Test
    public void test_get_display_info_at() {
        Coordinate upperLeft = new Coordinate(1, 2);
        int width = 1, height = 3;
        RectangleShip<Character> r = new RectangleShip<>(upperLeft, width, height, 's', '*');
        r.recordHitAt(new Coordinate(1, 2));
        r.recordHitAt(new Coordinate(3, 2));
        assertEquals("test-ship", r.getName());
        assertEquals('s', r.getDisplayInfoAt(new Coordinate(2, 2), true));
        assertEquals('*', r.getDisplayInfoAt(new Coordinate(1, 2), true));
        assertEquals('*', r.getDisplayInfoAt(new Coordinate(3, 2), true));
        assertEquals('s', r.getDisplayInfoAt(new Coordinate(3, 2), false));
        assertThrows(IllegalArgumentException.class, () -> r.getDisplayInfoAt(new Coordinate(4, 2), true));
    }

    @Test
    public void test_is_sunk() {
        Coordinate upperLeft = new Coordinate(1, 2);
        int width = 1, height = 3;
        RectangleShip<Character> r1 = new RectangleShip<>(upperLeft, width, height, 's', '*');
        r1.recordHitAt(new Coordinate(1, 2));
        r1.recordHitAt(new Coordinate(2, 2));
        r1.recordHitAt(new Coordinate(3, 2));
        assertEquals("test-ship", r1.getName());
        assertTrue(r1.isSunk());

        RectangleShip<Character> r2 = new RectangleShip<>(upperLeft, width, height, 's', '*');
        assertEquals("test-ship", r2.getName());
        assertFalse(r2.isSunk());
    }

    @Test
    public void test_get_coordinates() {
        Coordinate upperLeft = new Coordinate(1, 2);
        int width = 1, height = 3;
        RectangleShip<Character> r = new RectangleShip<>(upperLeft, width, height, 's', '*');
        Set<Coordinate> expected = new HashSet<>();
        expected.add(new Coordinate(1, 2));
        expected.add(new Coordinate(2, 2));
        expected.add(new Coordinate(3, 2));
        assertEquals(expected, r.getCoordinates());
    }
}