package edu.duke.rs590.battleship;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

class InBoundsRuleCheckerTest {

    @Test
    public void test_in_bounds_rule_checker() {
        // invalid
        InBoundsRuleChecker<Character> checker = new InBoundsRuleChecker<>(null);
        Board<Character> board = new BattleShipBoard<>(10, 20, 'X', checker);
        V1ShipFactory factory = new V1ShipFactory();
        // goes off the top of the board
        Placement p_top = new Placement(new Coordinate(-1, 0), 'H');
        // goes off the bottom of the board
        Placement p_bottom = new Placement(new Coordinate(21, 0), 'H');
        // goes off the right of the board
        Placement p_left = new Placement(new Coordinate(0, -1), 'H');
        // goes off the left of the board
        Placement p_right = new Placement(new Coordinate(0, 11), 'H');
        Ship<Character> ship_top = factory.makeSubmarine(p_top);
        Ship<Character> ship_bottom = factory.makeCarrier(p_bottom);
        Ship<Character> ship_left = factory.makeBattleship(p_left);
        Ship<Character> ship_right = factory.makeCarrier(p_right);
        assertNotNull(checker.checkPlacement(ship_top, board));
        assertNotNull(checker.checkPlacement(ship_bottom, board));
        assertNotNull(checker.checkPlacement(ship_left, board));
        assertNotNull(checker.checkPlacement(ship_right, board));

        // valid
        Placement p_valid = new Placement(new Coordinate(3, 3), 'H');
        Ship<Character> ship_valid = factory.makeSubmarine(p_valid);
        assertNull(checker.checkPlacement(ship_valid, board));
    }

}