package edu.duke.rs590.battleship;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class V2ShipFactoryTest extends V1ShipFactoryTest {
    V2ShipFactory factory = new V2ShipFactory();
    Placement up = new Placement(new Coordinate(0, 0), 'U');
    Placement right = new Placement(new Coordinate(0, 0), 'R');
    Placement down = new Placement(new Coordinate(0, 0), 'D');
    Placement left = new Placement(new Coordinate(0, 0), 'L');
    Placement h = new Placement(new Coordinate(0, 0), 'H');
    Placement v = new Placement(new Coordinate(0, 0), 'V');
    Placement unknown = new Placement(new Coordinate(0, 0), '.');

    @Test
    @Override
    void test_makeSubmarine() {
        Ship<Character> sub_H = factory.makeSubmarine(h);
        checkShip(sub_H, "Submarine", 's',
                new Coordinate(0, 0),
                new Coordinate(0, 1));
        Ship<Character> sub_V = factory.makeSubmarine(v);
        checkShip(sub_V, "Submarine", 's',
                new Coordinate(0, 0),
                new Coordinate(1, 0));
        assertThrows(IllegalArgumentException.class, () -> factory.makeSubmarine(unknown));
    }

    @Test
    @Override
    void test_makeDestroyer() {
        Ship<Character> des_H = factory.makeDestroyer(h);
        checkShip(des_H, "Destroyer", 'd',
                new Coordinate(0, 0),
                new Coordinate(0, 1),
                new Coordinate(0, 2));
        Ship<Character> des_V = factory.makeDestroyer(v);
        checkShip(des_V, "Destroyer", 'd',
                new Coordinate(0, 0),
                new Coordinate(1, 0),
                new Coordinate(2, 0));
        assertThrows(IllegalArgumentException.class, () -> factory.makeDestroyer(unknown));
    }

    @Test
    @Override
    void test_makeBattleship() {
        Ship<Character> bat_up = factory.makeBattleship(up);
        checkShip(bat_up, "Battleship", 'b',
                new Coordinate(1, 0),
                new Coordinate(0, 1),
                new Coordinate(1, 1),
                new Coordinate(1, 2));
        Ship<Character> bat_right = factory.makeBattleship(right);
        checkShip(bat_right, "Battleship", 'b',
                new Coordinate(0, 0),
                new Coordinate(1, 0),
                new Coordinate(2, 0),
                new Coordinate(1, 1));
        Ship<Character> bat_down = factory.makeBattleship(down);
        checkShip(bat_down, "Battleship", 'b',
                new Coordinate(0, 0),
                new Coordinate(0, 1),
                new Coordinate(0, 2),
                new Coordinate(1, 1));
        Ship<Character> bat_left = factory.makeBattleship(left);
        checkShip(bat_left, "Battleship", 'b',
                new Coordinate(0, 1),
                new Coordinate(1, 1),
                new Coordinate(2, 1),
                new Coordinate(1, 0));
        assertThrows(IllegalArgumentException.class, () -> factory.makeBattleship(unknown));
    }

    @Test
    @Override
    void test_makeCarrier() {
        Ship<Character> car_up = factory.makeCarrier(up);
        checkShip(car_up, "Carrier", 'c',
                new Coordinate(0, 0),
                new Coordinate(1, 0),
                new Coordinate(2, 0),
                new Coordinate(3, 0),
                new Coordinate(2, 1),
                new Coordinate(3, 1),
                new Coordinate(4, 1));
        Ship<Character> car_right = factory.makeCarrier(right);
        checkShip(car_right, "Carrier", 'c',
                new Coordinate(0, 1),
                new Coordinate(0, 2),
                new Coordinate(0, 3),
                new Coordinate(0, 4),
                new Coordinate(1, 0),
                new Coordinate(1, 1),
                new Coordinate(1, 2));
        Ship<Character> car_down = factory.makeCarrier(down);
        checkShip(car_down, "Carrier", 'c',
                new Coordinate(1, 1),
                new Coordinate(2, 1),
                new Coordinate(3, 1),
                new Coordinate(4, 1),
                new Coordinate(0, 0),
                new Coordinate(1, 0),
                new Coordinate(2, 0));
        Ship<Character> car_left = factory.makeCarrier(left);
        checkShip(car_left, "Carrier", 'c',
                new Coordinate(1, 0),
                new Coordinate(1, 1),
                new Coordinate(1, 2),
                new Coordinate(1, 3),
                new Coordinate(0, 2),
                new Coordinate(0, 3),
                new Coordinate(0, 4));
        assertThrows(IllegalArgumentException.class, () -> factory.makeCarrier(unknown));
    }

    @Test
    void test_invalid_name() {
        assertThrows(IllegalArgumentException.class, () -> factory.createShip(h, 1, 1, '1', "unknown"));
        assertThrows(IllegalArgumentException.class, () -> factory.createShip(up, '1', "unknown"));
    }

    @Test
    void test_to_String() {
        Ship<Character> car_up = factory.makeCarrier(up);
        String expected = " C1,C0,D1,E1,D0,B0,A0";
        assertEquals(expected, car_up.toString());
    }

}