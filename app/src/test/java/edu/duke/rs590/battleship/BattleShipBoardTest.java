package edu.duke.rs590.battleship;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class BattleShipBoardTest {
    @Test
    public void test_width_and_height(){
        Board<Character> b1 = new BattleShipBoard<>(10, 20, 'X');
        assertEquals(10, b1.getWidth());
        assertEquals(20, b1.getHeight());
    }

    @Test
    public void test_invalid_dimensions() {
        assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard<>(10, 0, 'X'));
        assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard<>(0, 20, 'X'));
        assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard<>(10, -5, 'X'));
        assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard<>(-8, 20, 'X'));
    }

    private <T> void checkWhatIsAtBoard(BattleShipBoard<T> b, T[][] expected) {
        for (int i = 0; i < b.getWidth(); i++) {
            for (int j = 0; j < b.getHeight(); j++) {
                assertEquals(b.whatIsAtForSelf(new Coordinate(i, j)), expected[i][j]);
            }
        }
    }

    @Test
    public void test_what_is_at_empty_board() {
        BattleShipBoard<Character> b1 = new BattleShipBoard<>(10, 20, 'X');
        Character[][] expected = new Character[10][20];
        checkWhatIsAtBoard(b1, expected);
    }

    @Test
    public void test_what_is_at_full_board() {
        BattleShipBoard<Character> b1 = new BattleShipBoard<>(10, 20, 'X');
        Character[][] expected = new Character[10][20];
        for (int i = 0; i < b1.getWidth(); i++) {
            for (int j = 0; j < b1.getHeight(); j++) {
                if (b1.tryAddShip(new RectangleShip<>(new Coordinate(i, j), 's', '*')) == null) {
                    expected[i][j] = 's';
                }
            }
        }
        checkWhatIsAtBoard(b1, expected);
    }

    @Test
    public void test_fire_at() {
        V1ShipFactory f = new V1ShipFactory();
        Board<Character> b1 = new BattleShipBoard<>(10, 20, 'X');
        Ship<Character> ship = f.makeSubmarine(new Placement(new Coordinate(0, 0), 'H'));
        b1.tryAddShip(ship);
        // hit the ship
        assertSame(ship, b1.fireAt(new Coordinate(0, 0)));
        // not sunk right now
        assertFalse(ship.isSunk());
        // miss hit
        assertNull(b1.fireAt(new Coordinate(6, 6)));
        // keep hit the ship
        assertSame(ship, b1.fireAt(new Coordinate(0, 1)));
        // the ship is sunk
        assertTrue(ship.isSunk());

    }

    @Test
    public void test_what_is_at_for_enemy() {
        V1ShipFactory f = new V1ShipFactory();
        Board<Character> b1 = new BattleShipBoard<>(10, 20, 'X');
        Ship<Character> ship = f.makeSubmarine(new Placement(new Coordinate(0, 0), 'H'));
        b1.tryAddShip(ship);
        // hit the ship
        b1.fireAt(new Coordinate(0, 0));
        // enemy should display ship's letter
        assertEquals('s', b1.whatIsAtForEnemy(new Coordinate(0, 0)));
        // this fire missed
        b1.fireAt(new Coordinate(0, 2));
        // enemy should display missInfo: 'X'
        assertEquals('X', b1.whatIsAtForEnemy(new Coordinate(0, 2)));
        // location is empty
        assertNull(b1.whatIsAtForEnemy(new Coordinate(0, 3)));
        // location has un-hit ship
        assertNull(b1.whatIsAtForEnemy(new Coordinate(0, 1)));
    }

    @Test
    public void test_has_lose() {
        V1ShipFactory f = new V1ShipFactory();
        Board<Character> b1 = new BattleShipBoard<>(10, 20, 'X');
        Ship<Character> ship = f.makeSubmarine(new Placement(new Coordinate(0, 0), 'H'));
        b1.tryAddShip(ship);
        // hit the ship
        b1.fireAt(new Coordinate(0, 0));
        // is not lose
        assertFalse(b1.isHasLost());
        // keep hit the ship
        b1.fireAt(new Coordinate(0, 1));
        // lost
        assertTrue(b1.isHasLost());
    }
}