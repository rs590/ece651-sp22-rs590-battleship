package edu.duke.rs590.battleship;

/**
 * This class handles all the constants for the project.
 */
public final class Constants {

    public static final String WELCOME_MSG = Utility.readFromResources("welcome_msg.txt");
    public static final String WELCOME_MSG_V2 = Utility.readFromResources("welcome_msg_v2.txt");
    public static final String WELCOME_MSG_V3 = Utility.readFromResources("welcome_msg_v3.txt");
    public static final String HEADER_FOOTER = Utility.readFromResources("header&footer.txt");
    public static final String PROMPT_PLACEMENT = Utility.readFromResources("prompt_placement.txt");
    public static final String PROMPT_PLACEMENT_V2 = Utility.readFromResources("prompt_placement_v2.txt");
    public static final String PROMPT_FIRE = Utility.readFromResources("prompt_fire.txt");
    public static final String PROMPT_SONAR = Utility.readFromResources("prompt_sonar.txt");
    public static final String PROMPT_ACTION_HEAD = "Possible actions for Player ";
    public static final String PROMPT_ACTION_FOOT = ", what would you like to do?";
    public static final String PROMPT_ACTION_FIRE = " F Fire at a square";
    public static final String PROMPT_ACTION_MOVE = " M Move a ship to another square ";
    public static final String PROMPT_ACTION_SONAR = " S Sonar scan ";
    public static final String TEST_PLACEMENT_INPUT = "placement_input.txt";
    public static final String TEST_INVALID_PLACEMENT_INPUT = "invalid_placement_input.txt";
    public static final String TEST_INVALID_PLACEMENT_OUTPUT = "invalid_placement_output.txt";
    public static final String TEST_PLACEMENT_OUTPUT = "placement_output.txt";
    public static final String TEST_MOVE_OUTPUT = "move_output.txt";
    public static final String TEST_SONAR_OUTPUT = "sonar_output.txt";
    public static final String INVALID_PLACEMENT = "That placement is invalid: ";
    public static final String INVALID_PLACEMENT_TOP = INVALID_PLACEMENT + "the ship goes off the top of the board.";
    public static final String INVALID_PLACEMENT_BOTTOM = INVALID_PLACEMENT + "the ship goes off the bottom of the board.";
    public static final String INVALID_PLACEMENT_LEFT = INVALID_PLACEMENT + "the ship goes off the left of the board.";
    public static final String INVALID_PLACEMENT_RIGHT = INVALID_PLACEMENT + "the ship goes off the right of the board.";
    public static final String INVALID_PLACEMENT_OVERLAP = INVALID_PLACEMENT + "the ship overlaps another ship.";
    public static final String YOU_MISSED = "You missed!";
    public static final String YOU_HIT = "You hit a ";
    public static final String MY_HEADER = "Your ocean";
    public static final String NEW_LINE = System.getProperty("line.separator");
    public static final String SPACE = " ";

    public static final int HEADER_SPACE_NUM = 5;
    public static final int BODY_SPACE_NUM = 16;
    public static final int CHOICE_NUM = 3;

    public static final long SEED = 651;
}
