package edu.duke.rs590.battleship;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

/**
 * This class handles the board of the battleship game
 */
public class BattleShipBoard<T> implements Board<T> {

    private boolean hasLost;
    private final int width;
    private final int height;
    private final T missInfo;
    private final PlacementRuleChecker<T> placementChecker;
    private final ArrayList<Ship<T>> myShips;
    private final HashSet<Coordinate> enemyMisses;
    private final HashMap<Coordinate, T> enemyHit;

    /**
     * Constructs a BattleShipBoard with the specific
     * Width and Height
     *
     * @param width  is the width of the newly constructed board.
     * @param height is the height of the newly constructed board.
     * @throws IllegalArgumentException if the width or height are less than or equal to zero.
     */
    public BattleShipBoard(int width, int height, T missInfo,
                           PlacementRuleChecker<T> placementChecker) {
        if (width <= 0) {
            throw new IllegalArgumentException("" +
                    "BattleShipBoard's width must be positive but is " + width);
        }
        if (height <= 0) {
            throw new IllegalArgumentException("" +
                    "BattleShipBoard's height must be positive but is " + height);
        }
        this.width = width;
        this.height = height;
        this.hasLost = false;
        this.missInfo = missInfo;
        this.myShips = new ArrayList<>();
        this.enemyMisses = new HashSet<>();
        this.enemyHit = new HashMap<>();
        this.placementChecker = placementChecker;
    }

    /**
     * Constructs a BattleShipBoard with default rule checker.
     *
     * @param w is the width of the newly constructed board.
     * @param h is the height of the newly constructed board.
     */
    public BattleShipBoard(int w, int h, T missInfo) {
        this(w, h, missInfo, new InBoundsRuleChecker<>(new NoCollisionRuleChecker<>(null)));
    }

    /**
     * Try to add the ship into myShips list.
     *
     * @param toAdd the ship to add.
     * @return null if successfully added the ship;
     * return the fail message otherwise.
     */
    public String tryAddShip(Ship<T> toAdd) {
        String msg = placementChecker.checkPlacement(toAdd, this);
        if (msg == null) {
            myShips.add(toAdd);
        }
        return msg;
    }

    /**
     * Try to move the old ship to new location.
     *
     * @param toMove the old ship.
     * @param moved  the new ship created.
     * @return null if successfully added the ship;
     * return the fail message otherwise.
     */
    public String tryMoveShip(Ship<T> toMove, Ship<T> moved) {
        myShips.remove(toMove);
        String msg = tryAddShip(moved);
        if (msg == null) {
            moved.updatePieces(toMove);
            return null;
        } else {
            tryAddShip(toMove);
            return msg;
        }
    }

    /**
     * Check which type of ship is at current coordinate
     *
     * @param where  is the coordinate to check
     * @param isSelf is whether the own or the enemy board to check
     * @return the type of the ship
     */
    protected T whatIsAt(Coordinate where, boolean isSelf) {
        Ship<T> ship = whatShipIsAt(where);
        if (ship != null) {
            return ship.getDisplayInfoAt(where, isSelf);
        }
        return null;
    }

    /**
     * Check which type of ship is at current coordinate (Own view)
     *
     * @param where is the coordinate to check
     * @return the type of the ship
     */
    public T whatIsAtForSelf(Coordinate where) {
        return whatIsAt(where, true);
    }

    /**
     * Check which type of ship is at current coordinate (Enemy view)
     *
     * @param where is the coordinate to check
     * @return the type of the ship
     */
    public T whatIsAtForEnemy(Coordinate where) {
        if (enemyMisses.contains(where)) {
            return missInfo;
        } else if (enemyHit.containsKey(where)) {
            return enemyHit.get(where);
        }
        return null;
    }

    /**
     * Helper function to check what ship is at current coordinate
     *
     * @param where is the coordinate to check
     * @return the object to the ship itself
     */
    public Ship<T> whatShipIsAt(Coordinate where) {
        for (Ship<T> s : myShips) {
            if (s.occupiesCoordinates(where)) {
                return s;
            }
        }
        return null;
    }

    /**
     * Fire at the coordinate.
     *
     * @param c is the coordinate fire at.
     * @return the hit ship, if hit.
     */
    public Ship<T> fireAt(Coordinate c) {
        Ship<T> hit_ship = whatShipIsAt(c);
        if (hit_ship != null) {
            hit_ship.recordHitAt(c);
            enemyHit.put(c, whatIsAt(c, false));
            checkLose();
        } else {
            enemyMisses.add(c);
        }
        return hit_ship;
    }

    /**
     * Check whether all the ship is sunk.
     * If so, the player is lost.
     */
    private void checkLose() {
        for (Ship<T> ship : myShips) {
            if (!ship.isSunk()) {
                return;
            }
        }
        hasLost = true;
    }

    /**
     * Getters
     **/

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }

    public boolean isHasLost() {
        return hasLost;
    }

    public ArrayList<Ship<T>> getMyShips() {
        return myShips;
    }
}
