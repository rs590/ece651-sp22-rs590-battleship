package edu.duke.rs590.battleship.server;

import edu.duke.rs590.battleship.Constants;
import edu.duke.rs590.battleship.Utility;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ServerApp {

    public static void main(String[] args) throws IOException {
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
        System.out.print(Constants.WELCOME_MSG_V3 + Constants.NEW_LINE);
        // set up server
        Server server = Server.getInstance(Utility.readPortNum(input));
        server.waitAllPlayersConnected();
        server.choosePlayerType();
        server.doPlacementPhase();
        server.doAttackingPhase();
        // close server
        server.close();
    }
}
