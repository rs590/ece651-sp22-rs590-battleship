package edu.duke.rs590.battleship;

import java.util.TreeMap;

/**
 * This class is the abstraction of the rectangle ships.
 */
public class RectangleShip<T> extends BasicShip<T> {
    final String name;

    /**
     * Constructs a rectangle ship.
     *
     * @param upperLeft     is the upper left coordinate of the ship.
     * @param width         is the width of the ship.
     * @param height        is the width of the ship.
     * @param myDisplayInfo is the display info of the ship.
     */
    public RectangleShip(String name, Coordinate upperLeft, int width, int height, ShipDisplayInfo<T> myDisplayInfo,
                         ShipDisplayInfo<T> enemyDisplayInfo) {
        super(makeCoords(upperLeft, width, height), myDisplayInfo, enemyDisplayInfo);
        this.name = name;
    }

    /**
     * Constructs a rectangle ship.
     *
     * @param upperLeft is the upper left coordinate of the ship.
     * @param width is the width of the ship.
     * @param height is the width of the ship.
     * @param data is the info that how to display a normal ship
     * @param onHit is the info that how tho display a ship which been hit
     */
    public RectangleShip(String name, Coordinate upperLeft, int width, int height, T data, T onHit) {
        this(name, upperLeft, width, height, new SimpleShipDisplayInfo<>(data, onHit),
                new SimpleShipDisplayInfo<>(null, data));
    }

    /**
     *  Constructors for test.
     */
    public RectangleShip(Coordinate upperLeft, T data, T onHit) {
        this("test-ship", upperLeft, 1, 1, data, onHit);
    }

    public RectangleShip(Coordinate upperLeft, int width, int height, T data, T onHit) {
        this("test-ship", upperLeft, width, height, new SimpleShipDisplayInfo<>(data, onHit),
                new SimpleShipDisplayInfo<>(null, data));
    }

    /**
     * This makes the coordinates of a rectangle ship,
     *  column(w)
     *  ---------->
     * r|  0 1 2 3
     * o|A
     * w|B
     * hv
     * @param upperLeft is the upper left coordinate of a ship
     * @param width is the width of a ship
     * @param height is the height of a ship
     * @return the set of coordinates occupied by the ship
     */
    static TreeMap<Integer, Coordinate> makeCoords(Coordinate upperLeft, int width, int height) {
        TreeMap<Integer, Coordinate> ans = new TreeMap<>();
        int idx = 1;
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                ans.put(idx++, new Coordinate(upperLeft.getRow() + j, upperLeft.getColumn() + i));
            }
        }
        return ans;
    }

    /**
     * Getter
     **/
    @Override
    public String getName() {
        return name;
    }

    /**
     * Override the toString() function.
     *
     * @return the converted String of the ship.
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        String sep = " ";
        for (Coordinate c : myPieces.keySet()) {
            sb.append(sep);
            char row_chr = (char) (c.getRow() + 65);
            sb.append(row_chr).append(c.getColumn());
            sep = ",";
        }
        return sb.toString();
    }
}
