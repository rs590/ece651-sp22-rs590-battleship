package edu.duke.rs590.battleship;

import java.util.TreeMap;

/**
 * This class is the abstraction of the irregular ships.
 */
public class IrregularShip<T> extends BasicShip<T> {
    final String name;

    /**
     * Constructs a rectangle ship.
     *
     * @param name             is the name of the ship.
     * @param placement        is the placement of the ship.
     * @param myDisplayInfo    is the display info of the self view of ship.
     * @param enemyDisplayInfo is the display info of the enemy's view of ship.
     */
    public IrregularShip(String name, Placement placement,
                         ShipDisplayInfo<T> myDisplayInfo,
                         ShipDisplayInfo<T> enemyDisplayInfo) {
        super(makeCoords(name, placement), myDisplayInfo, enemyDisplayInfo);
        this.name = name;
    }

    public IrregularShip(String name, Placement placement, T data, T onHit) {
        this(name, placement, new SimpleShipDisplayInfo<>(data, onHit),
                new SimpleShipDisplayInfo<>(null, data));
    }

    /**
     * This makes the coordinates of an irregular ship
     *
     * @param name      is the name of the ship.
     * @param placement is the placement of the ship.
     * @return the set of coordinates occupied by the ship.
     */
    static TreeMap<Integer, Coordinate> makeCoords(String name, Placement placement) {
        return switch (name) {
            case "Battleship" -> makeBattleshipCoords(placement);
            case "Carrier" -> makeCarrierCoords(placement);
            default -> throw new IllegalArgumentException("Unknown ship name :" + name);
        };
    }

    /**
     * Call different make coordinates functions based on the orientation,
     * to make the coordinates for Battleship.
     *
     * @param placement is the placement of the ship.
     * @return the set of coordinates occupied by the ship.
     */
    static TreeMap<Integer, Coordinate> makeBattleshipCoords(Placement placement) {
        return switch (placement.getOrientation()) {
            case 'U' -> makeBattleshipUpCoords(placement.getCoordinate());
            case 'R' -> makeBattleshipRightCoords(placement.getCoordinate());
            case 'D' -> makeBattleshipDownCoords(placement.getCoordinate());
            case 'L' -> makeBattleshipLeftCoords(placement.getCoordinate());
            default -> throw new IllegalArgumentException("Unknown orientation type : "
                    + placement.getOrientation());
        };
    }

    /**
     * Call different make coordinates functions based on the orientation,
     * to make the coordinates for Carrier.
     *
     * @param placement is the placement of the ship.
     * @return the set of coordinates occupied by the ship.
     */
    static TreeMap<Integer, Coordinate> makeCarrierCoords(Placement placement) {
        return switch (placement.getOrientation()) {
            case 'U' -> makeCarrierUpCoords(placement.getCoordinate());
            case 'R' -> makeCarrierRightCoords(placement.getCoordinate());
            case 'D' -> makeCarrierDownCoords(placement.getCoordinate());
            case 'L' -> makeCarrierLeftCoords(placement.getCoordinate());
            default -> throw new IllegalArgumentException("Unknown orientation type : "
                    + placement.getOrientation());
        };
    }

    /**
     * Make the coordinates for Battleship with Up orientation.
     * e.g.,(* is the coordinate of the placement where)
     * *b
     * bbb
     *
     * @param where specifies the location and orientation of the ship to make
     * @return the set of coordinates occupied by the ship
     */
    static TreeMap<Integer, Coordinate> makeBattleshipUpCoords(Coordinate where) {
        int idx = 1;
        TreeMap<Integer, Coordinate> ans = new TreeMap<>();
        ans.put(idx++, new Coordinate(where.getRow(), where.getColumn() + 1));
        for (int i = 0; i < 3; i++) {
            ans.put(idx++, new Coordinate(where.getRow() + 1, where.getColumn() + i));
        }
        return ans;
    }

    /**
     * Make the coordinates for Battleship with Right orientation.
     * e.g.,(B is the coordinate of the placement where)
     * B
     * bb
     * b
     *
     * @param where specifies the location and orientation of the ship to make
     * @return the set of coordinates occupied by the ship
     */
    static TreeMap<Integer, Coordinate> makeBattleshipRightCoords(Coordinate where) {
        int idx = 1;
        TreeMap<Integer, Coordinate> ans = new TreeMap<>();
        ans.put(idx++, new Coordinate(where.getRow() + 1, where.getColumn() + 1));
        for (int i = 0; i < 3; i++) {
            ans.put(idx++, new Coordinate(where.getRow() + i, where.getColumn()));
        }
        return ans;
    }

    /**
     * Make the coordinates for Battleship with Down orientation.
     * e.g.,(B is the coordinate of the placement where)
     * Bbb
     * /b
     *
     * @param where specifies the location and orientation of the ship to make
     * @return the set of coordinates occupied by the ship
     */
    static TreeMap<Integer, Coordinate> makeBattleshipDownCoords(Coordinate where) {
        int idx = 1;
        TreeMap<Integer, Coordinate> ans = new TreeMap<>();
        ans.put(idx++, new Coordinate(where.getRow() + 1, where.getColumn() + 1));
        for (int i = 0; i < 3; i++) {
            ans.put(idx++, new Coordinate(where.getRow(), where.getColumn() + i));
        }
        return ans;
    }

    /**
     * Make the coordinates for Battleship with Left orientation.
     * e.g.,(* is the coordinate of the placement where)
     * *b
     * bb
     * /b
     *
     * @param where specifies the location and orientation of the ship to make
     * @return the set of coordinates occupied by the ship
     */
    static TreeMap<Integer, Coordinate> makeBattleshipLeftCoords(Coordinate where) {
        int idx = 1;
        TreeMap<Integer, Coordinate> ans = new TreeMap<>();
        ans.put(idx++, new Coordinate(where.getRow() + 1, where.getColumn()));
        for (int i = 0; i < 3; i++) {
            ans.put(idx++, new Coordinate(where.getRow() + i, where.getColumn() + 1));
        }
        return ans;
    }

    /**
     * Make the coordinates for Carrier with Up orientation.
     * e.g.,(C is the coordinate of the placement where)
     * C
     * c
     * cc
     * cc
     * /c
     *
     * @param where specifies the location and orientation of the ship to make
     * @return the set of coordinates occupied by the ship
     */
    static TreeMap<Integer, Coordinate> makeCarrierUpCoords(Coordinate where) {
        int idx = 1;
        TreeMap<Integer, Coordinate> ans = new TreeMap<>();
        for (int i = 0; i < 4; i++) {
            ans.put(idx++, new Coordinate(where.getRow() + i, where.getColumn()));
        }
        for (int i = 0; i < 3; i++) {
            ans.put(idx++, new Coordinate(where.getRow() + 2 + i, where.getColumn() + 1));
        }
        return ans;
    }

    /**
     * Make the coordinates for Carrier with Right orientation.
     * e.g.,(* is the coordinate of the placement where)
     * *cccc
     * ccc
     *
     * @param where specifies the location and orientation of the ship to make
     * @return the set of coordinates occupied by the ship
     */
    static TreeMap<Integer, Coordinate> makeCarrierRightCoords(Coordinate where) {
        int idx = 1;
        TreeMap<Integer, Coordinate> ans = new TreeMap<>();
        for (int i = 0; i < 4; i++) {
            ans.put(idx++, new Coordinate(where.getRow(), where.getColumn() + 1 + i));
        }
        for (int i = 0; i < 3; i++) {
            ans.put(idx++, new Coordinate(where.getRow() + 1, where.getColumn() + i));
        }
        return ans;
    }

    /**
     * Make the coordinates for Carrier with Down orientation.
     * e.g.,(* is the coordinate of the placement where)
     * C
     * cc
     * cc
     * /c
     * /c
     *
     * @param where specifies the location and orientation of the ship to make
     * @return the set of coordinates occupied by the ship
     */
    static TreeMap<Integer, Coordinate> makeCarrierDownCoords(Coordinate where) {
        int idx = 1;
        TreeMap<Integer, Coordinate> ans = new TreeMap<>();
        for (int i = 0; i < 4; i++) {
            ans.put(idx++, new Coordinate(where.getRow() + 1 + i, where.getColumn() + 1));
        }
        for (int i = 0; i < 3; i++) {
            ans.put(idx++, new Coordinate(where.getRow() + i, where.getColumn()));
        }
        return ans;
    }

    /**
     * Make the coordinates for Carrier with Left orientation.
     * e.g.,(* is the coordinate of the placement where)
     * * ccc
     * cccc
     *
     * @param where specifies the location and orientation of the ship to make
     * @return the set of coordinates occupied by the ship
     */
    static TreeMap<Integer, Coordinate> makeCarrierLeftCoords(Coordinate where) {
        int idx = 1;
        TreeMap<Integer, Coordinate> ans = new TreeMap<>();
        for (int i = 0; i < 4; i++) {
            ans.put(idx++, new Coordinate(where.getRow() + 1, where.getColumn() + i));
        }
        for (int i = 0; i < 3; i++) {
            ans.put(idx++, new Coordinate(where.getRow(), where.getColumn() + 2 + i));
        }
        return ans;
    }

    /*Getter*/
    @Override
    public String getName() {
        return name;
    }

    /**
     * Override the toString() function.
     *
     * @return the converted String of the ship.
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        String sep = " ";
        for (Coordinate c : myPieces.keySet()) {
            sb.append(sep);
            char row_chr = (char) (c.getRow() + 65);
            sb.append(row_chr).append(c.getColumn());
            sep = ",";
        }
        return sb.toString();
    }
}
