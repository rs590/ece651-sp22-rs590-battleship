package edu.duke.rs590.battleship;

/**
 * This class is responsible for checking whether the ship
 * is in the bound of the board.
 */
public class InBoundsRuleChecker<T> extends PlacementRuleChecker<T> {

    public InBoundsRuleChecker(PlacementRuleChecker<T> next) {
        super(next);
    }

    /**
     * Check whether the ship placement in the bounds of the board.
     *
     * @param theShip  is the ship to check.
     * @param theBoard is the board to check.
     * @return true if meets the requirements.
     */
    @Override
    protected String checkMyRule(Ship<T> theShip, Board<T> theBoard) {
        for (Coordinate coordinate : theShip.getCoordinates()) {
            if (coordinate.getRow() < 0) {
                return Constants.INVALID_PLACEMENT_TOP + Constants.NEW_LINE;
            } else if (coordinate.getRow() >= theBoard.getHeight()) {
                return Constants.INVALID_PLACEMENT_BOTTOM + Constants.NEW_LINE;
            } else if (coordinate.getColumn() < 0) {
                return Constants.INVALID_PLACEMENT_LEFT + Constants.NEW_LINE;
            } else if (coordinate.getColumn() >= theBoard.getWidth()) {
                return Constants.INVALID_PLACEMENT_RIGHT + Constants.NEW_LINE;
            }
        }
        return null;
    }
}
