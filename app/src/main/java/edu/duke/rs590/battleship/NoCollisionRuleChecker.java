package edu.duke.rs590.battleship;

public class NoCollisionRuleChecker<T> extends PlacementRuleChecker<T> {

    public NoCollisionRuleChecker(PlacementRuleChecker<T> next) {
        super(next);
    }

    /**
     * Check whether the ship placement collide other ship.
     *
     * @param theShip  is the ship to check.
     * @param theBoard is the board to check.
     * @return true if meets the requirements.
     */
    @Override
    protected String checkMyRule(Ship<T> theShip, Board<T> theBoard) {
        for (Coordinate coordinate : theShip.getCoordinates()) {
            if (theBoard.whatIsAtForSelf(coordinate) != null) {
                return Constants.INVALID_PLACEMENT_OVERLAP + Constants.NEW_LINE;
            }
        }
        return null;
    }
}
