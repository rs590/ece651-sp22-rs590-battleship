package edu.duke.rs590.battleship.client;

import edu.duke.rs590.battleship.Utility;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;

public class Client extends Thread {
    private final int PORT;
    private final String HOST;
    private Socket socket;
    private InputStream input;
    private OutputStream output;

    /**
     * construction of Client
     * Use initClient method to init client.
     */
    public Client(String HOST, int PORT) {
        this.HOST = HOST;
        this.PORT = PORT;
        initClient();
    }

    /**
     * construction of Client
     * Use initClient method to init client.
     */
    public Client() {
        this.HOST = "localhost";
        this.PORT = 3024;
        initClient();
    }

    // Read
    @Override
    public void run() {
        // Write
        new sendMessThread().start();
        super.run();
        try {
            InputStream s = socket.getInputStream();
            byte[] buf = new byte[1024];
            int len = 0;
            while ((len = s.read(buf)) != -1) {
                System.out.print(new String(buf, 0, len));
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Used to create connection with server by socket.
     */
    private void initClient() {
        try {
            InetAddress ip = InetAddress.getByName(HOST);
            this.socket = new Socket(ip, PORT);
            this.input = this.socket.getInputStream();
            this.output = this.socket.getOutputStream();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void choosePlayerType() throws IOException {
        String type = Utility.readPlayerType("COM or HUMAN?", new BufferedReader(new InputStreamReader(System.in)));
        Utility.sendMessage(type, output);
    }

    public void close() {
        try {
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Socket getSocket() {
        return socket;
    }

    // Write
    class sendMessThread extends Thread {
        @Override
        public void run() {
            super.run();
            try {
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(output));
                String in;
                do {
                    in = Utility.readInput(new BufferedReader(new InputStreamReader(System.in)));
                    System.out.println(in);
                    writer.write(in);
                    writer.newLine();
                    writer.flush();
                } while (!in.equals("bye"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
