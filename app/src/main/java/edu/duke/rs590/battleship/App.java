package edu.duke.rs590.battleship;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * This class handles whole battleship game (Version 1)
 */
public class App {

    final TextPlayer player1;
    final TextPlayer player2;

    /**
     * Construct app with two player.
     *
     * @param player1 is the first player.
     * @param player2 is the first player.
     */
    public App(TextPlayer player1, TextPlayer player2) {
        this.player1 = player1;
        this.player2 = player2;
    }

    /**
     * Main function for Battleship V2.
     *
     * @param args String[] parameters
     * @throws IOException if something wrong with I/O, e.g., EOF
     */
    public static void main(String[] args) throws IOException {
        Board<Character> b1 = new BattleShipBoard<>(10, 20, 'X');
        Board<Character> b2 = new BattleShipBoard<>(10, 20, 'X');
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
        V1ShipFactory factory = new V1ShipFactory();
        TextPlayer player1 = new TextPlayer("A", b1, input, System.out, factory);
        TextPlayer player2 = new TextPlayer("B", b2, input, System.out, factory);
        System.out.println(Constants.WELCOME_MSG);
        App app = new App(player1, player2);
        app.doPlacementPhase();
        app.doAttackingPhase();
    }

    /**
     * Do the entire placement phase for both players.
     *
     * @throws IOException if there is something wrong with the I/O
     */
    public void doPlacementPhase() throws IOException {
        player1.doPlacementPhase();
        player2.doPlacementPhase();
    }

    /**
     * Do the attacking phase.
     *
     * @throws IOException if there is something wrong with the I/O.
     */
    public void doAttackingPhase() throws IOException {
        while (true) {
            player1.playOneTurn(player2);
            if (player2.theBoard.isHasLost()) {
                player2.beDefeatedBy(player1);
                break;
            }
            player2.playOneTurn(player1);
            if (player1.theBoard.isHasLost()) {
                player1.beDefeatedBy(player2);
                break;
            }
        }
    }


}
