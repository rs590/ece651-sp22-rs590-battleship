package edu.duke.rs590.battleship;

/**
 * This class handles the simple display info of the ship.
 */
public class SimpleShipDisplayInfo<T> implements ShipDisplayInfo<T> {
    private final T myData;
    private final T onHit;

    public SimpleShipDisplayInfo(T myData, T onHit) {
        this.myData = myData;
        this.onHit = onHit;
    }

    @Override
    public T getInfo(Coordinate where, boolean hit) {
        if (hit) {
            return onHit;
        }
        return myData;
    }
}
