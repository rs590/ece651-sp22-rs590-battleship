package edu.duke.rs590.battleship;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;

/**
 * This class is the abstraction of the text player (version 2).
 */
public class V2TextPlayer extends TextPlayer {
    protected int move_remain = 3;
    protected int sonar_remain = 3;

    /**
     * Constructs a Text player.
     *
     * @param name        is the name of player.
     * @param theBoard    is the board of the player.
     * @param inputReader is the input reader of the player.
     * @param out         is the output source of the player.
     * @param shipFactory is the ship factory fo the player.
     */
    public V2TextPlayer(String name, Board<Character> theBoard, BufferedReader inputReader, PrintStream out,
                        AbstractShipFactory<Character> shipFactory) {
        super(name, theBoard, inputReader, out, shipFactory);
    }

    /**
     * Do the entire placement phase for the players.
     *
     * @throws IOException if there is something wrong with the I/O
     */
    @Override
    public void doPlacementPhase() throws IOException {
        // Display the empty board
        out.print(Constants.HEADER_FOOTER + view.displayMyOwnBoard());
        // print the instruction message
        out.print(Constants.HEADER_FOOTER + "Player " + name + Constants.PROMPT_PLACEMENT_V2);
        // do placement
        for (String ship : shipsToPlace) {
            doOnePlacement(ship, shipCreationFns.get(ship));
        }
    }

    /**
     * Play one single turn of the game.
     *
     * @param enemy is the enemy player.
     */
    public void playOneTurn(V2TextPlayer enemy) throws IOException {
        // display the turn prompt
        out.print(Constants.HEADER_FOOTER + "Player " + name + "'s turn:" + Constants.NEW_LINE);
        // make enemy's header
        String enemyHeader = "Player " + enemy.name + "'s ocean";
        // display current turn's board
        out.print(view.displayMyBoardWithEnemyNextToIt(enemy.view, Constants.MY_HEADER, enemyHeader));
        if (move_remain > 0 || sonar_remain > 0) {
            makeChoice(enemy);
        } else {
            fireAction(enemy);
        }

    }

    /**
     * Do the fire action.
     *
     * @param enemy is the enemy to fire.
     * @throws IOException if there is something wrong with the I/O
     */
    public void fireAction(V2TextPlayer enemy) throws IOException {
        // reade the fire coordinate
        Coordinate toFire = readCoordinate();
        // make the fire
        Ship<Character> hit_ship = enemy.theBoard.fireAt(toFire);
        if (hit_ship != null) {
            out.print(Constants.HEADER_FOOTER + Constants.YOU_HIT + hit_ship.getName() + "!" + Constants.NEW_LINE);
        } else {
            out.print(Constants.HEADER_FOOTER + Constants.YOU_MISSED + Constants.NEW_LINE);
        }
    }

    /**
     * Do the move action.
     *
     * @param enemy is the enemy to play with.
     * @throws IOException if there is something wrong with the I/O
     */
    public void moveAction(V2TextPlayer enemy) throws IOException {
        // list all the ships with its coordinates
        out.print(Constants.HEADER_FOOTER + "Movable Ships:" + Constants.NEW_LINE);
        for (Ship<Character> ship : theBoard.getMyShips()) {
            if (!ship.isSunk()) {
                out.print(ship.getName() + ship + Constants.NEW_LINE);
            }
        }
        out.print(Constants.NEW_LINE + "Sunken Ships:" + Constants.NEW_LINE);
        for (Ship<Character> ship : theBoard.getMyShips()) {
            if (ship.isSunk()) {
                out.print(ship.getName() + Constants.NEW_LINE);
            }
        }
        // display select ship prompt
        out.print(Constants.HEADER_FOOTER +
                "Please select a movable ship, by input a coordinate of the ship." + Constants.NEW_LINE);
        // find the ship selected by the player
        Ship<Character> ship_selected = theBoard.whatShipIsAt(readCoordinate());
        // if selection is invalid, go back to action choice
        if (ship_selected == null || ship_selected.isSunk()) {
            out.print("Invalid coordinate, back to action choice." + Constants.NEW_LINE);
            makeChoice(enemy);
            return;
        }
        // display move prompt
        out.print(Constants.HEADER_FOOTER +
                "Where do you want to move the ship?" + Constants.NEW_LINE);
        // get the placement from input
        Placement new_placement = readPlacement("Please input a placement.");
        // produce the new ship
        Ship<Character> new_ship = shipCreationFns.get(ship_selected.getName()).apply(new_placement);
        // try to add the new ship to the board
        String msg = theBoard.tryMoveShip(ship_selected, new_ship);
        if (msg != null) {
            out.print(msg + Constants.NEW_LINE);
            out.print("Invalid move, back to action choice." + Constants.NEW_LINE);
            makeChoice(enemy);
            return;
        }
        // move success
        move_remain--;
    }

    /**
     * Do the sonar scan action.
     *
     * @param enemy is the enemy to play with.
     * @throws IOException if there is something wrong with the I/O
     */
    public void sonarAction(V2TextPlayer enemy) throws IOException {
        // Display the prompt for sonar
        out.print(Constants.HEADER_FOOTER + Constants.PROMPT_SONAR);
        // Get the centre coordinate
        Coordinate centre = readCoordinate();
        // Put all the type of ship in to a hash map, initial the num to 0
        HashMap<String, Integer> sonarRes = new HashMap<>() {
            {
                for (Ship<Character> ship : enemy.theBoard.getMyShips()) {
                    put(ship.getName(), 0);
                }
            }
        };
        // Do sonar scan
        for (int row = centre.getRow() - 3; row < centre.getRow() + 3; row++) {
            if (row < 0 || row > enemy.theBoard.getHeight()) {
                continue;
            }
            int offset = 3 - Math.abs(row - centre.getRow());
            for (int col = centre.getColumn() - offset; col < centre.getColumn() + 3; col++) {
                if (col < 0 || col > enemy.theBoard.getWidth()) {
                    continue;
                }
                Ship<Character> ship_found = enemy.theBoard.whatShipIsAt(new Coordinate(row, col));
                if (ship_found != null) {
                    sonarRes.put(ship_found.getName(), sonarRes.get(ship_found.getName()) + 1);
                }
            }
        }
        // Display the result
        out.print(Constants.HEADER_FOOTER);
        for (Map.Entry<String, Integer> entry : sonarRes.entrySet()) {
            out.print(entry.getKey() + " occupy " + entry.getValue() + (entry.getValue() > 1 ? " squares" : " square") +
                    Constants.NEW_LINE);
        }
        // sonar action end
        sonar_remain--;
    }

    /**
     * Let player make choice of the actions.
     *
     * @param enemy is the enemy to play with.
     * @throws IOException if there is something wrong with the I/O.
     */
    public void makeChoice(V2TextPlayer enemy) throws IOException {
        out.print(Constants.HEADER_FOOTER + Constants.PROMPT_ACTION_HEAD + name + ":" + Constants.NEW_LINE);
        out.print(Constants.NEW_LINE + Constants.PROMPT_ACTION_FIRE + Constants.NEW_LINE);
        out.print(Constants.PROMPT_ACTION_MOVE + "(" + move_remain + " remaining)" + Constants.NEW_LINE);
        out.print(Constants.PROMPT_ACTION_SONAR + "(" + sonar_remain + " remaining)" + Constants.NEW_LINE);
        out.print(Constants.NEW_LINE + getName() + Constants.PROMPT_ACTION_FOOT + Constants.NEW_LINE);
        // read user's choice
        String choice = readChoice();
        // switch to different actions
        if (choice.equals("M") && move_remain <= 0) {
            out.print("Invalid choice." + Constants.NEW_LINE);
            makeChoice(enemy);
            return;
        }

        if (choice.equals("S") && sonar_remain <= 0) {
            out.print("Invalid choice." + Constants.NEW_LINE);
            makeChoice(enemy);
            return;
        }

        switch (choice) {
            case "F" -> fireAction(enemy);
            case "M" -> moveAction(enemy);
            case "S" -> sonarAction(enemy);
        }
    }

    /**
     * Read choice from input.
     *
     * @return the choice from input
     * @throws IOException is there is something wrong with the I/O, e.g. EOF
     */
    public String readChoice() throws IOException {
        String ans = null;
        boolean flag = true;
        while (flag) {
            out.print(Constants.HEADER_FOOTER + "Please input 'F'('f'), 'M'('m') or 'S'(s)." +
                    Constants.NEW_LINE);
            String s = Utility.readInput(inputReader).toUpperCase();
            try {
                switch (s) {
                    case "F", "M", "S" -> ans = s;
                    default -> throw new IllegalArgumentException("Invalid input: Unknown action.");
                }
                flag = false;
            } catch (IllegalArgumentException e) {
                out.println(e.getMessage());
                out.println("Please try again.");
            }
        }
        return ans;
    }
}

