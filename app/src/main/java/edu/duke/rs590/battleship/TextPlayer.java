package edu.duke.rs590.battleship;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.function.Function;

/**
 * This class is the abstraction of the text player.
 * The interaction between game and the player is based on text.
 */
public class TextPlayer {
    protected String name;
    public Board<Character> theBoard;
    protected BufferedReader inputReader;
    protected PrintStream out;
    protected AbstractShipFactory<Character> shipFactory;
    final BoardTextView view;
    final ArrayList<String> shipsToPlace;
    final HashMap<String, Function<Placement, Ship<Character>>> shipCreationFns;

    /**
     * Constructs a Text player.
     *
     * @param name        is the name of player.
     * @param theBoard    is the board of the player.
     * @param inputReader is the input reader of the player.
     * @param out         is the output source of the player.
     * @param shipFactory is the ship factory fo the player.
     */
    public TextPlayer(String name, Board<Character> theBoard, BufferedReader inputReader, PrintStream out,
                      AbstractShipFactory<Character> shipFactory) {
        this.name = name;
        this.theBoard = theBoard;
        this.inputReader = inputReader;
        this.out = out;
        this.shipFactory = shipFactory;
        this.view = new BoardTextView(theBoard);
        this.shipsToPlace = new ArrayList<>();
        this.shipCreationFns = new HashMap<>();
        setupShipCreationMap();
        setupShipCreationList();
    }

    /**
     * Set up the filed: shipCreationFns
     */
    protected void setupShipCreationMap() {
        shipCreationFns.put("Submarine", shipFactory::makeSubmarine);
        shipCreationFns.put("Battleship", shipFactory::makeBattleship);
        shipCreationFns.put("Carrier", shipFactory::makeCarrier);
        shipCreationFns.put("Destroyer", shipFactory::makeDestroyer);
    }

    /**
     * Set up the filed: shipsToPlace
     */
    protected void setupShipCreationList() {
        shipsToPlace.addAll(Collections.nCopies(2, "Submarine"));
        shipsToPlace.addAll(Collections.nCopies(3, "Destroyer"));
        shipsToPlace.addAll(Collections.nCopies(3, "Battleship"));
        shipsToPlace.addAll(Collections.nCopies(2, "Carrier"));
    }

    /**
     * Read the placement from input.
     *
     * @param prompt is the prompt of the input
     * @return the placement obj parse from the input
     * @throws IOException if there is something wrong with the I/O
     */
    public Placement readPlacement(String prompt) throws IOException {
        Placement placement = null;
        boolean flag = true;
        while (flag) {
            out.print(prompt + Constants.NEW_LINE);
            String s = Utility.readInput(inputReader);
            try {
                placement = new Placement(s);
                flag = false;
            } catch (IllegalArgumentException e) {
                out.println(e.getMessage());
                out.println("Please try again.");
                out.print(Constants.HEADER_FOOTER);
            }
        }
        return placement;
    }

    /**
     * Read the coordinate from input.
     *
     * @return the coordinate obj parse from the input
     * @throws IOException if there is something wrong with the I/O
     */
    public Coordinate readCoordinate() throws IOException {
        Coordinate coordinate = null;
        boolean flag = true;
        while (flag) {
            out.print(Constants.HEADER_FOOTER + "Please input a coordinate, like(a0, C9...)"
                    + Constants.NEW_LINE);
            String s = Utility.readInput(inputReader);
            try {
                coordinate = new Coordinate(s);
                if (coordinate.getRow() >= theBoard.getHeight() ||
                        coordinate.getColumn() >= theBoard.getWidth()) {
                    throw new IllegalArgumentException("Invalid Coordinate: Out of board.");
                }
                flag = false;
            } catch (IllegalArgumentException e) {
                out.println(e.getMessage());
                out.println("Please try again.");
            }
        }
        return coordinate;
    }

    /**
     * Do the placement based on the input.
     *
     * @throws IOException if there is something wrong with the I/O
     */
    public void doOnePlacement(String shipName, Function<Placement, Ship<Character>> createFn) throws IOException {
        while (true) {
            out.print(Constants.HEADER_FOOTER);
            String prompt = getName() + " where do you want to place a " + shipName + "?";
            Placement placement = readPlacement(prompt);
            // @throw Exception
            try {
                Ship<Character> basicShip = createFn.apply(placement);
                String msg = theBoard.tryAddShip(basicShip);
                if (msg == null) {
                    out.print(Constants.HEADER_FOOTER + view.displayMyOwnBoard());
                    break;
                }
                out.print(msg);
                out.println("Please try again.");
            } catch (IllegalArgumentException e) {
                out.println(e.getMessage());
                out.println("Please try again.");
            }
        }
    }

    /**
     * Do the entire placement phase for the players.
     *
     * @throws IOException if there is something wrong with the I/O
     */
    public void doPlacementPhase() throws IOException {
        // Display the empty board
        out.print(Constants.HEADER_FOOTER + view.displayMyOwnBoard());
        // print the instruction message
        out.print(Constants.HEADER_FOOTER + getName() + Constants.PROMPT_PLACEMENT);
        // do placement
        for (String ship : shipsToPlace) {
            doOnePlacement(ship, shipCreationFns.get(ship));
        }
    }

    /**
     * Play one single turn of the game.
     *
     * @param enemy is the enemy player.
     */
    public void playOneTurn(TextPlayer enemy) throws IOException {
        // display the turn prompt
        out.print(Constants.HEADER_FOOTER + getName() + "'s turn:" + Constants.NEW_LINE);
        // make enemy's header
        String enemyHeader = enemy.getName() + "'s ocean";
        // display current turn's board
        out.print(view.displayMyBoardWithEnemyNextToIt(enemy.view, Constants.MY_HEADER, enemyHeader));
        // display the prompt for fire action
        out.print(Constants.HEADER_FOOTER + Constants.PROMPT_FIRE);
        // reade the fire coordinate
        Coordinate toFire = readCoordinate();
        // make the fire
        Ship<Character> hit_ship = enemy.theBoard.fireAt(toFire);
        if (hit_ship != null) {
            out.print(Constants.HEADER_FOOTER + Constants.YOU_HIT + hit_ship.getName() + "!" + Constants.NEW_LINE);
        } else {
            out.print(Constants.HEADER_FOOTER + Constants.YOU_MISSED + Constants.NEW_LINE);
        }
    }

    /**
     * Out put the defeated message.
     *
     * @param enemy is the player who defeat current player
     */
    public void beDefeatedBy(TextPlayer enemy) {
        out.print(Constants.HEADER_FOOTER + enemy.getName() + " WIN!\n" + Constants.HEADER_FOOTER);
    }

    public String getName() {
        return "Player " + name;
    }
}
