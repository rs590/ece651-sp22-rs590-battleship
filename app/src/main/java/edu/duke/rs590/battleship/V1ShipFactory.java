package edu.duke.rs590.battleship;

/**
 * This class is the ship factory of Version1.
 */
public class V1ShipFactory implements AbstractShipFactory<Character> {
    // Hit Mark
    final char onHit = '*';

    /**
     * Abstraction of creat ships.
     *
     * @param where the location of the ship.
     * @param w the width of the ship.
     * @param h the height of the ship.
     * @param letter the notation of the ship.
     * @param name the name of the ship.
     * @return the new ship.
     */
    protected Ship<Character> createShip(Placement where, int w, int h, char letter, String name) {
        if (where.getOrientation() == 'V') {
            return new RectangleShip<>(name, where.getCoordinate(), w, h, letter, onHit);
        }  else if (where.getOrientation() == 'H') {
            return new RectangleShip<>(name, where.getCoordinate(), h, w, letter, onHit);
        } else {
            throw new IllegalArgumentException("The orientation must be 'V'('v') or 'H'('h')" +
                    "but is " + where.getOrientation());
        }
    }

    /**
     * Make a submarine.
     *
     * @param where specifies the location and orientation of the ship to make
     * @return the Ship created for the submarine.
     */
    @Override
    public Ship<Character> makeSubmarine(Placement where) {
        String name = "Submarine";
        int width = 1, height = 2;
        char data = 's';
        return createShip(where, width, height, data, name);
    }

    /**
     * Make a battleship.
     *
     * @param where specifies the location and orientation of the ship to make
     * @return the Ship created for the battleship.
     */
    @Override
    public Ship<Character> makeBattleship(Placement where) {
        String name = "Battleship";
        int width = 1, height = 4;
        char data = 'b';
        return createShip(where, width, height, data, name);
    }

    /**
     * Make a carrier.
     *
     * @param where specifies the location and orientation of the ship to make
     * @return the Ship created for the carrier.
     */
    @Override
    public Ship<Character> makeCarrier(Placement where) {
        String name = "Carrier";
        int width = 1, height = 6;
        char data = 'c';
        return createShip(where, width, height, data, name);
    }

    /**
     * Make a destroyer.
     *
     * @param where specifies the location and orientation of the ship to make
     * @return the Ship created for the destroyer.
     */
    @Override
    public Ship<Character> makeDestroyer(Placement where) {
        String name = "Destroyer";
        int width = 1, height = 3;
        char data = 'd';
        return createShip(where, width, height, data, name);
    }
}
