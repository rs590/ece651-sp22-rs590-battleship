package edu.duke.rs590.battleship;


import java.util.function.Function;

/**
 * This class handles textual display of
 * a Board (i.e., converting it to a string to show
 * to the user).
 * It supports two ways to display the Board:
 * one for the player's own board, and one for the
 * enemy's board.
 */
public class BoardTextView {
    /**
     * The Board to display
     */
    private final Board<Character> toDisplay;

    /**
     * Constructs a BoardView, given the board it will display.
     *
     * @param toDisplay is the Board to display.
     * @throws IllegalArgumentException if the board is larger than 10x26.
     */
    public BoardTextView(Board<Character> toDisplay) {
        this.toDisplay = toDisplay;
        if (toDisplay.getWidth() > 10 || toDisplay.getHeight() > 26) {
            throw new IllegalArgumentException(
                    "Board must be no larger than 10x26, but is " + toDisplay.getWidth() + "x" +
                            toDisplay.getHeight()
            );
        }
    }

    /**
     * This display any text view of board.
     *
     * @param getSquareFn is the lambada function to chose which board to display.
     * @return the String that is the whole board.
     */
    protected String displayAnyBoard(Function<Coordinate, Character> getSquareFn) {
        return makeHeader() + makeBody(getSquareFn) + makeHeader();
    }

    /**
     * This display the own text view of board.
     *
     * @return the String that is the whole board.
     */
    public String displayMyOwnBoard() {
        return displayAnyBoard(toDisplay::whatIsAtForSelf);
    }

    /**
     * This display the enemy text view of board.
     *
     * @return the String that is the whole board.
     */
    public String displayEnemyBoard() {
        return displayAnyBoard(toDisplay::whatIsAtForEnemy);
    }

    /**
     * This makes the header line, e.g. 0|1|2|3|4\n
     *
     * @return the String that is the header line for the given board.
     */
    String makeHeader() {
        StringBuilder ans = new StringBuilder("  ");
        String sep = "";
        for (int i = 0; i < toDisplay.getWidth(); i++) {
            ans.append(sep);
            ans.append(i);
            sep = "|";
        }
        ans.append("\n");
        return ans.toString();
    }

    /**
     * This makes the body line, e.g., A  | |  A\n
     *
     * @param getSquareFn lambda function to chose which info to display
     * @return the String that is the body line for the given board.
     */
    String makeBody(Function<Coordinate, Character> getSquareFn) {
        StringBuilder ans = new StringBuilder();
        for (int i = 0; i < toDisplay.getHeight(); i++) {
            ans.append((char) (65 + i));
            ans.append(" ");
            String sep = "";
            for (int j = 0; j < toDisplay.getWidth(); j++) {
                ans.append(sep);
                char ship = getSquareFn.apply(new Coordinate(i, j)) == null ?
                        ' ' : getSquareFn.apply(new Coordinate(i, j));
                ans.append(ship);
                sep = "|";
            }
            ans.append(" ");
            ans.append((char) (65 + i));
            ans.append("\n");
        }
        return ans.toString();
    }

    /**
     * This display two text view of boards.
     *
     * @param enemyView   is the enemy's view.
     * @param myHeader    is the header of own.
     * @param enemyHeader is the enemy;s header.
     * @return the combined two boards.
     */
    public String displayMyBoardWithEnemyNextToIt(BoardTextView enemyView, String myHeader, String enemyHeader) {
        // get width and height of my board
        int width = toDisplay.getWidth(), height = toDisplay.getHeight();
        // make sure that two board has same width and height
        assert (enemyView.toDisplay.getWidth() == width &&
                enemyView.toDisplay.getHeight() == height);
        // get the String of two boards
        String myBoard = this.displayMyOwnBoard();
        String enemyBoard = enemyView.displayEnemyBoard();
        StringBuilder sb = new StringBuilder();

        // split two String by "\n"
        String[] myBoard_array = myBoard.split("\n");
        String[] enemyBoard_array = enemyBoard.split("\n");
        enemyBoard_array[0] = "  " + enemyBoard_array[0];
        enemyBoard_array[enemyBoard_array.length - 1] = "  " + enemyBoard_array[enemyBoard_array.length - 1];

        // make header
        sb.append(Constants.SPACE.repeat(Constants.HEADER_SPACE_NUM));
        sb.append(myHeader);
        sb.append(Constants.SPACE.repeat(Math.max(0, 2 * width + 17 - myHeader.length())));
        sb.append(enemyHeader).append(Constants.NEW_LINE);

        // make body
        // make sure that both array has same length
        assert (myBoard_array.length == enemyBoard_array.length);
        for (int i = 0; i < myBoard_array.length; i++) {
            sb.append(myBoard_array[i]);
            sb.append(Constants.SPACE.repeat(Constants.BODY_SPACE_NUM));
            sb.append(enemyBoard_array[i]).append(Constants.NEW_LINE);
        }

        return sb.toString();
    }
}
