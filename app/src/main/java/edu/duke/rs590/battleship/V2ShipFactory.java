package edu.duke.rs590.battleship;

/**
 * This class is the ship factory of Version1.
 */
public class V2ShipFactory extends V1ShipFactory {

    /**
     * Abstraction of creat ship in version 2.
     *
     * @param where  the location of the ship.
     * @param w      the width of the ship.
     * @param h      the height of the ship.
     * @param letter the notation of the ship.
     * @param name   the name of the ship.
     * @return the ship created.
     */
    @Override
    protected Ship<Character> createShip(Placement where, int w, int h, char letter, String name) {
        return switch (name) {
            case "Submarine", "Destroyer" -> super.createShip(where, w, h, letter, name);
            default -> throw new IllegalArgumentException("The ship name is unknown.");
        };
    }

    /**
     * Abstraction of creat ship in version 2.
     *
     * @param where  the location of the ship.
     * @param letter the notation of the ship.
     * @param name   the name of the ship.
     * @return the ship created.
     */
    protected Ship<Character> createShip(Placement where, char letter, String name) {
        return switch (name) {
            case "Battleship", "Carrier" -> new IrregularShip<>(name, where, letter, onHit);
            default -> throw new IllegalArgumentException("The ship name is unknown.");
        };
    }

    /**
     * Make a battleship.
     *
     * @param where specifies the location and orientation of the ship to make
     * @return the Ship created for the battleship.
     */
    @Override
    public Ship<Character> makeBattleship(Placement where) {
        String name = "Battleship";
        char data = 'b';
        return createShip(where, data, name);
    }

    /**
     * Make a carrier.
     *
     * @param where specifies the location and orientation of the ship to make
     * @return the Ship created for the carrier.
     */
    @Override
    public Ship<Character> makeCarrier(Placement where) {
        String name = "Carrier";
        char data = 'c';
        return createShip(where, data, name);
    }
}
