package edu.duke.rs590.battleship;

import java.util.ArrayList;

/**
 * This interface handles the board model
 */
public interface Board<T> {

    /**
     * Try to add the ship into myShips list.
     *
     * @param toAdd the ship to add.
     * @return null if successfully added the ship;
     * return the fail message otherwise.
     */
    String tryAddShip(Ship<T> toAdd);

    /**
     * Try to move the old ship to new location.
     *
     * @param toMove the old ship.
     * @param moved  the new ship created.
     * @return null if successfully added the ship;
     * return the fail message otherwise.
     */
    String tryMoveShip(Ship<T> toMove, Ship<T> moved);

    /**
     * Check which type of ship is at current coordinate (Own view)
     *
     * @param where is the coordinate to check
     * @return the type of the ship
     */
    T whatIsAtForSelf(Coordinate where);

    /**
     * Check which type of ship is at current coordinate (Enemy view)
     *
     * @param where is the coordinate to check
     * @return the type of the ship
     */
    T whatIsAtForEnemy(Coordinate where);

    /**
     * Fire at the coordinate.
     *
     * @param c is the coordinate fire at.
     * @return the hit ship, if hit.
     */
    Ship<T> fireAt(Coordinate c);

    /**
     * Helper function to check what ship is at current coordinate
     *
     * @param where is the coordinate to check
     * @return the object to the ship itself
     */
    Ship<T> whatShipIsAt(Coordinate where);

    /**
     * Getters
     **/

    int getWidth();

    int getHeight();

    boolean isHasLost();

    ArrayList<Ship<T>> getMyShips();
}
