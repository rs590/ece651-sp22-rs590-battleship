package edu.duke.rs590.battleship;

import java.io.*;
import java.util.HashMap;
import java.util.Random;

/**
 * This class handles all the utility functions.
 */
public final class Utility {

    private Utility() {
    }

    /**
     * Read file from resources.
     *
     * @param fileName is the file name under resources.
     * @return content of the file.
     */
    public static String readFromResources(String fileName) {
        StringBuilder res = new StringBuilder();
        try {
            InputStream input = Utility.class.getClassLoader().getResourceAsStream(fileName);
            BufferedReader reader = new BufferedReader(new InputStreamReader(input));
            String line;
            String newLine = System.getProperty("line.separator");
            boolean flag = false;
            while ((line = reader.readLine()) != null) {
                res.append(flag ? newLine : "").append(line);
                flag = true;
            }
            res.append(newLine);
        } catch (Exception e) {
            System.err.println("[Utility]Something wrong with I/O.");
        }
        return res.toString();
    }

    /**
     * Read input from BufferedReader.
     *
     * @param inputReader is the input reader.
     * @return the String parse from input.
     * @throws IOException if something wrong with I/O.
     */
    public static String readInput(BufferedReader inputReader) throws IOException {
        String str = inputReader.readLine();
        if (str == null) {
            throw new EOFException("[Terminate] EOF");
        }
        return str;
    }

    /**
     * Read player type from System.in.
     *
     * @param prompt is the prompt for input.
     * @return String "COM" or "HUMAN"
     * @throws IOException is something wrong with I/O, e.g., EOF
     */
    public static String readPlayerType(String prompt, BufferedReader inputReader) throws IOException {
        while (true) {
            System.out.print(Constants.HEADER_FOOTER + prompt + Constants.NEW_LINE);
            String s = Utility.readInput(inputReader);
            if (s.equalsIgnoreCase("COM") || s.equalsIgnoreCase("HUMAN")) {
                return s.toUpperCase();
            } else {
                System.out.print("Invalid input, please enter again." + Constants.NEW_LINE);
            }
        }
    }

    public static int readPortNum(BufferedReader inputReader) throws IOException {
        while (true) {
            System.out.println("Which port do you want to set up the server?");
            System.out.println("Enter \"0\" will automatically assign the port.");
            int port = Integer.parseInt(Utility.readInput(inputReader));
            if (port == 0 || (port >= 1024 && port <= 65535)) {
                return port;
            } else {
                System.out.print("Invalid input, please enter again." + Constants.NEW_LINE);
                System.out.println("Port number should between 1024 and 65535 or 0.");
            }
        }
    }

    public static String readHostName(BufferedReader inputReader) throws IOException {
        while (true) {
            System.out.println("Enter the hostname you want to connect.");
            String hostname = Utility.readInput(inputReader);
            if (!hostname.isBlank()) {
                return hostname;
            } else {
                System.out.print("Invalid input, please enter again." + Constants.NEW_LINE);
            }
        }
    }

    public static int readPortNumForClient(BufferedReader inputReader) throws IOException {
        while (true) {
            System.out.println("Enter the port number you want to connect.");
            int port = Integer.parseInt(Utility.readInput(inputReader));
            if (port >= 1024 && port <= 65535) {
                return port;
            } else {
                System.out.print("Invalid input, please enter again." + Constants.NEW_LINE);
                System.out.println("Port number should between 1024 and 65535.");
            }
        }
    }

    public static void sendMessage(String msg, OutputStream output) {
        try {
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(output));
            writer.write(msg);
            writer.newLine();
            writer.flush();
        } catch (IOException e) {
            return;
        }
    }

    public static String recvMessage(InputStream input) {
        String str = null;
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(input));
//            String tmp;
//            while(((tmp = reader.readLine()) != null)){
//                str += tmp;
//            }
            str = reader.readLine();
        } catch (IOException e) {
            return null;
        }
        assert str != null;
        return str;
    }

    /**
     * Generate Random Orientation for Random Text player.
     *
     * @param isRectangleShip is the orientation for rectangle ship.
     * @return Character of the orientation.
     */
    public static Character getRandomOrientation(boolean isRectangleShip, Random randomGenerator) {
        int range = isRectangleShip ? 2 : 4;
        int random = randomGenerator.nextInt(range);

        HashMap<Integer, Character> rectangleOrientations = new HashMap<>() {
            {
                put(0, 'V');
                put(1, 'H');
            }
        };
        HashMap<Integer, Character> irregularOrientations = new HashMap<>() {
            {
                put(0, 'U');
                put(1, 'R');
                put(2, 'D');
                put(3, 'L');
            }
        };

        if (isRectangleShip) {
            return rectangleOrientations.get(random);
        } else {
            return irregularOrientations.get(random);
        }
    }

    /**
     * Generate random number.
     *
     * @param range is the range of the random number.
     * @return the generated random number.
     */
    public static int getRandomNumber(int range, Random randomGenerator) {
        return randomGenerator.nextInt(range);
    }

}
