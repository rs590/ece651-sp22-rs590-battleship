package edu.duke.rs590.battleship.server;

import edu.duke.rs590.battleship.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Server {
    public static Server server;        //singleton server instance
    private final int playerNum = 2;
    private ServerSocket serverSocket;  //listen socket
    private List<Socket> connections; //sockets

    private V2TextPlayer player1;
    private V2TextPlayer player2;
    private Board<Character> b1;
    private Board<Character> b2;
    private V2ShipFactory factory;
    private Random randomGenerator;

    /**
     * Singleton server constructor
     *
     * @param port is the port to listen for connect
     */
    private Server(int port) {
        connections = new ArrayList<>();
        b1 = new BattleShipBoard<>(10, 20, 'X');
        b2 = new BattleShipBoard<>(10, 20, 'X');
        factory = new V2ShipFactory();
        randomGenerator = new Random(Constants.SEED);
        initSever(port);
    }

    /**
     * Get singleton server instance
     *
     * @return Singleton server instance
     */
    public static Server getInstance(int port) {
        if (server == null) {
            server = new Server(port);
        }

        return server;
    }

    /**
     * Init the server, bind the port, accept all connections from player
     *
     * @param port is the port to listen for connect
     */
    private void initSever(int port) {
        try {
            this.serverSocket = new ServerSocket(port); //bind port
            System.out.println("[The server is set up on port: " + getPort() + "]");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void waitAllPlayersConnected() {
        for (int i = 0; i < playerNum; ++i) {
            Socket socket;
            try {
                socket = serverSocket.accept();
                System.out.println("Client connected");
                this.connections.add(socket);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    void choosePlayerType() {
        try {
            String type = Utility.recvMessage(connections.get(0).getInputStream());
            System.out.println(type);
            if (type != null && type.equalsIgnoreCase("COM")) {
                player1 = new RandomTextPlayer("A", b1, new BufferedReader(new InputStreamReader(connections.get(0).getInputStream())),
                        new PrintStream(connections.get(0).getOutputStream(), true), factory, randomGenerator);
            } else if (type != null && type.equalsIgnoreCase("HUMAN")) {
                player1 = new V2TextPlayer("A", b1, new BufferedReader(new InputStreamReader(connections.get(0).getInputStream())),
                        new PrintStream(connections.get(0).getOutputStream(), true), factory);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            String type = Utility.recvMessage(connections.get(1).getInputStream());
            System.out.println(type);
            if (type != null && type.equalsIgnoreCase("COM")) {
                player2 = new RandomTextPlayer("B", b2, new BufferedReader(new InputStreamReader(connections.get(1).getInputStream())),
                        new PrintStream(connections.get(1).getOutputStream(), true), factory, randomGenerator);
            } else if (type != null && type.equalsIgnoreCase("HUMAN")) {
                player2 = new V2TextPlayer("B", b2, new BufferedReader(new InputStreamReader(connections.get(1).getInputStream())),
                        new PrintStream(connections.get(1).getOutputStream(), true), factory);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Do the entire placement phase for both players.
     *
     * @throws IOException if there is something wrong with the I/O
     */
    public void doPlacementPhase() throws IOException {
        player1.doPlacementPhase();
        player2.doPlacementPhase();
    }

    /**
     * Do the attacking phase.
     *
     * @throws IOException if there is something wrong with the I/O.
     */
    public void doAttackingPhase() throws IOException {
        while (true) {
            player1.playOneTurn(player2);
            if (player2.theBoard.isHasLost()) {
                player2.beDefeatedBy(player1);
                break;
            }
            player2.playOneTurn(player1);
            if (player1.theBoard.isHasLost()) {
                player1.beDefeatedBy(player2);
                break;
            }
        }
    }

    void close() {
        try {
            this.serverSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    int getPort() {
        return serverSocket.getLocalPort();
    }
}
