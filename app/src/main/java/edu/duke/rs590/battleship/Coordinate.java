package edu.duke.rs590.battleship;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class handles the coordinates of the game
 */
public class Coordinate {
    private final int row, column;

    /**
     * Constructs a coordinate.
     *
     * @param row    is the index of the row
     * @param column is the index of the column
     */
    public Coordinate(int row, int column) {
        this.row = row;
        this.column = column;
    }

    /**
     * Constructs a coordinate by a descr.
     *
     * @param descr is the String to describe a coordinate
     */
    public Coordinate(String descr) {
        // Using regx
        String pattern = "^[A-Za-z][0-9]$";
        Pattern regex = Pattern.compile(pattern);
        Matcher matcher = regex.matcher(descr);
        // invalid cases
        if (descr.length() != 2 || !matcher.matches()) {
            throw new IllegalArgumentException("Coordinate must be two characters: A-Z(a-z) and 0-9, " +
                    "but is " + descr);
        }

        char row_chr = descr.toUpperCase().charAt(0);
        char col_chr = descr.charAt(1);
        // ASCII 65->A, 48->1
        this.row = row_chr - 65;
        this.column = col_chr - 48;
    }

    /**
     * Override the equals() function.
     *
     * @param o the obj to compare.
     * @return ture if the two obj are equal.
     */
    @Override
    public boolean equals(Object o) {
        if (o.getClass().equals(getClass())) {
            Coordinate coordinate = (Coordinate) o;
            return row == coordinate.row && column == coordinate.column;
        }
        return false;
    }

    /**
     * Override the hashCode() function.
     *
     * @return the hashCode of the object.
     */
    @Override
    public int hashCode() {
        return toString().hashCode();
    }

    /**
     * Override the toString function.
     *
     * @return the converted String of the coordinate.
     */
    @Override
    public String toString() {
        return "(" + row + ", " + column + ")";
    }

    /**
     * Getters
     **/

    public int getColumn() {
        return column;
    }

    public int getRow() {
        return row;
    }
}
