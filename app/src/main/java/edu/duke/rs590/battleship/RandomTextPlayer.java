package edu.duke.rs590.battleship;

import java.io.BufferedReader;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.Random;
import java.util.function.Function;

public class RandomTextPlayer extends V2TextPlayer {
    final String action_prompt = getName() + " used a special action.";
    final Random randomGenerator;

    /**
     * Constructs a Random Text player.
     *
     * @param name        is the name of player.
     * @param theBoard    is the board of the player.
     * @param inputReader is the input reader of the player.
     * @param out         is the output source of the player.
     * @param shipFactory is the ship factory fo the player.
     */
    public RandomTextPlayer(String name, Board<Character> theBoard, BufferedReader inputReader,
                            PrintStream out, AbstractShipFactory<Character> shipFactory,
                            Random randomGenerator) {
        super(name, theBoard, inputReader, out, shipFactory);
        this.randomGenerator = randomGenerator;
    }

    public Placement makeRandomPlacement(boolean isRectangle) {
        Placement placement = null;
        boolean flag = true;
        while (flag) {
            Coordinate randomCoordinate = makeRandomCoordinate();
            Character randomOrientation = Utility.getRandomOrientation(isRectangle, randomGenerator);
            assert randomOrientation != null;
            placement = new Placement(randomCoordinate, randomOrientation);
            flag = false;
        }
        return placement;
    }

    public Coordinate makeRandomCoordinate() {
        Coordinate coordinate = null;
        boolean flag = true;
        while (flag) {
            int randomRow = Utility.getRandomNumber(theBoard.getHeight(), randomGenerator);
            int randomCol = Utility.getRandomNumber(theBoard.getWidth(), randomGenerator);
            coordinate = new Coordinate(randomRow, randomCol);
            flag = false;
        }
        return coordinate;
    }

    @Override
    public void doOnePlacement(String shipName, Function<Placement, Ship<Character>> createFn) {
        while (true) {
            out.print(getName() + " is doing placement." + Constants.NEW_LINE);
            boolean isRectangle = switch (shipName) {
                case "Submarine", "Destroyer" -> true;
                default -> false;
            };
            Placement placement = makeRandomPlacement(isRectangle);
            Ship<Character> basicShip = createFn.apply(placement);
            String msg = theBoard.tryAddShip(basicShip);
            if (msg == null) {
                break;
            }
        }
    }

    @Override
    public void doPlacementPhase() {
        // do placement
        for (String ship : shipsToPlace) {
            doOnePlacement(ship, shipCreationFns.get(ship));
        }
    }

    @Override
    public void playOneTurn(V2TextPlayer enemy) {
        if (move_remain > 0 || sonar_remain > 0) {
            makeChoice(enemy);
        } else {
            fireAction(enemy);
        }

    }

    @Override
    public void makeChoice(V2TextPlayer enemy) {
        out.print(Constants.HEADER_FOOTER + getName() + " is making choice." + Constants.NEW_LINE);
        int randomChoice = Utility.getRandomNumber(Constants.CHOICE_NUM, randomGenerator);
        switch (randomChoice) {
            case 0 -> fireAction(enemy);
            case 1 -> moveAction(enemy);
            case 2 -> sonarAction(enemy);
        }
    }

    @Override
    public void fireAction(V2TextPlayer enemy) {
        // reade the fire coordinate
        Coordinate toFire = makeRandomCoordinate();
        StringBuilder coor_sb = new StringBuilder();
        char row_chr = (char) (toFire.getRow() + 65);
        coor_sb.append(row_chr).append(toFire.getColumn());
        // make the fire
        Ship<Character> hit_ship = enemy.theBoard.fireAt(toFire);
        if (hit_ship != null) {
            out.print(Constants.HEADER_FOOTER + getName() + " hit a " + hit_ship.getName() + " at "
                    + coor_sb + "." + Constants.NEW_LINE);
        } else {
            out.print(Constants.HEADER_FOOTER + getName() + " missed." + Constants.NEW_LINE);
        }
    }

    @Override
    public void moveAction(V2TextPlayer enemy) {
        // find the ship selected by the player
        Ship<Character> ship_selected = theBoard.whatShipIsAt(makeRandomCoordinate());
        // if selection is invalid, go back to action choice
        if (ship_selected == null || ship_selected.isSunk()) {
            makeChoice(enemy);
            return;
        }
        // whether the ship is rectangle
        boolean isRectangle = switch (ship_selected.getName()) {
            case "Submarine", "Destroyer" -> true;
            default -> false;
        };
        // get the placement from input
        Placement new_placement = makeRandomPlacement(isRectangle);
        // produce the new ship
        Ship<Character> new_ship = shipCreationFns.get(ship_selected.getName()).apply(new_placement);
        // try to add the new ship to the board
        String msg = theBoard.tryMoveShip(ship_selected, new_ship);
        if (msg != null) {
            makeChoice(enemy);
            return;
        }
        // move success
        out.print(Constants.HEADER_FOOTER + action_prompt + Constants.NEW_LINE);
        move_remain--;
    }

    @Override
    public void sonarAction(V2TextPlayer enemy) {
        // Get the centre coordinate
        Coordinate centre = makeRandomCoordinate();
        // Put all the type of ship in to a hash map, initial the num to 0
        HashMap<String, Integer> sonarRes = new HashMap<>() {
            {
                for (Ship<Character> ship : enemy.theBoard.getMyShips()) {
                    put(ship.getName(), 0);
                }
            }
        };
        // Do sonar scan
        for (int row = centre.getRow() - 3; row < centre.getRow() + 3; row++) {
            if (row < 0 || row > enemy.theBoard.getHeight()) {
                continue;
            }
            int offset = 3 - Math.abs(row - centre.getRow());
            for (int col = centre.getColumn() - offset; col < centre.getColumn() + 3; col++) {
                if (col < 0 || col > enemy.theBoard.getWidth()) {
                    continue;
                }
                Ship<Character> ship_found = enemy.theBoard.whatShipIsAt(new Coordinate(row, col));
                if (ship_found != null) {
                    sonarRes.put(ship_found.getName(), sonarRes.get(ship_found.getName()) + 1);
                }
            }
        }
        // sonar action end
        out.print(Constants.HEADER_FOOTER + action_prompt + Constants.NEW_LINE);
        sonar_remain--;
    }

}
