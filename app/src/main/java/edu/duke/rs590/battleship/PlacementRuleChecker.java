package edu.duke.rs590.battleship;

public abstract class PlacementRuleChecker<T> {

    private final PlacementRuleChecker<T> next;

    /**
     * Constructs a placement rule checker.
     *
     * @param next is the link to next rule checker.
     */
    public PlacementRuleChecker(PlacementRuleChecker<T> next) {
        this.next = next;
    }

    /**
     * Check the specific placement rule.
     *
     * @param theShip  is the ship to check.
     * @param theBoard is the board to check.
     * @return true if meets the requirements.
     */
    protected abstract String checkMyRule(Ship<T> theShip, Board<T> theBoard);

    public String checkPlacement(Ship<T> theShip, Board<T> theBoard) {
        //if we fail our own rule: stop the placement is not legal
        String msg = checkMyRule(theShip, theBoard);
        if (msg != null) {
            return msg;
        }
        //otherwise, ask the rest of the chain.
        if (next != null) {
            msg = next.checkPlacement(theShip, theBoard);
        }
        //if there are no more rules, then the placement is legal
        return msg;
    }
}
