package edu.duke.rs590.battleship;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;

/**
 * This class handles whole battleship game (Version 2)
 */
public class V2App {

    final V2TextPlayer player1;
    final V2TextPlayer player2;

    /**
     * Construct app with two player.
     *
     * @param player1 is the first player.
     * @param player2 is the first player.
     */
    public V2App(V2TextPlayer player1, V2TextPlayer player2) {
        this.player1 = player1;
        this.player2 = player2;
    }

    /**
     * Main function for Battleship V2.
     *
     * @param args String[] parameters
     * @throws IOException if something wrong with I/O, e.g., EOF
     */
    public static void main(String[] args) throws IOException {
        Board<Character> b1 = new BattleShipBoard<>(10, 20, 'X');
        Board<Character> b2 = new BattleShipBoard<>(10, 20, 'X');
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
        V2ShipFactory factory = new V2ShipFactory();
        Random randomGenerator = new Random(Constants.SEED);
        V2TextPlayer player1 = null;
        V2TextPlayer player2 = null;
        System.out.print(Constants.WELCOME_MSG_V2 + Constants.NEW_LINE);
        switch (Utility.readPlayerType("Player 1 is Com/Human ?", input)) {
            case "COM" -> player1 = new RandomTextPlayer("A", b1, input, System.out, factory, randomGenerator);
            case "HUMAN" -> player1 = new V2TextPlayer("A", b1, input, System.out, factory);
        }
        switch (Utility.readPlayerType("Player 2 is Com/Human ?", input)) {
            case "COM" -> player2 = new RandomTextPlayer("B", b2, input, System.out, factory, randomGenerator);
            case "HUMAN" -> player2 = new V2TextPlayer("B", b2, input, System.out, factory);
        }
        V2App app = new V2App(player1, player2);
        app.doPlacementPhase();
        app.doAttackingPhase();
    }

    /**
     * Do the entire placement phase for both players.
     *
     * @throws IOException if there is something wrong with the I/O
     */
    public void doPlacementPhase() throws IOException {
        player1.doPlacementPhase();
        player2.doPlacementPhase();
    }

    /**
     * Do the attacking phase.
     *
     * @throws IOException if there is something wrong with the I/O.
     */
    public void doAttackingPhase() throws IOException {
        while (true) {
            player1.playOneTurn(player2);
            if (player2.theBoard.isHasLost()) {
                player2.beDefeatedBy(player1);
                break;
            }
            player2.playOneTurn(player1);
            if (player1.theBoard.isHasLost()) {
                player1.beDefeatedBy(player2);
                break;
            }
        }
    }
}
