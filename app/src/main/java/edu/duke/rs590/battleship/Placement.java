package edu.duke.rs590.battleship;

/**
 * This class handles the placement of the battleships.
 */
public class Placement {
    private final Coordinate coordinate;
    private final char orientation;

    /**
     * Constructs a placement.
     *
     * @param coordinate is the coordinate to place.
     * @param orientation is the orientation to place.
     */
    public Placement(Coordinate coordinate, char orientation) {
        this.coordinate = coordinate;
        this.orientation = Character.toUpperCase(orientation);
    }

    /**
     * Constructs a placement by a descr.
     *
     * @param descr is the String to describe a placement.
     */
    public Placement(String descr) {
        if (descr.length() != 3) {
            throw new IllegalArgumentException("That placement is invalid: it does not have the correct format.");
        }
        this.coordinate = new Coordinate(descr.substring(0, 2).toUpperCase());
        this.orientation = descr.toUpperCase().charAt(2);
    }

    @Override
    public boolean equals(Object o) {
        if (o.getClass().equals(getClass())) {
            Placement p = (Placement) o;
            return p.coordinate.equals(coordinate) && p.orientation == orientation;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return toString().hashCode();
    }

    @Override
    public String toString() {
        return coordinate.toString() + ", " + orientation;
    }

    public Coordinate getCoordinate() {
        return coordinate;
    }

    public char getOrientation() {
        return orientation;
    }
}
