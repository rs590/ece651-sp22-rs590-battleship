package edu.duke.rs590.battleship;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;

public abstract class BasicShip<T> implements Ship<T> {

    /**
     * if myPieces.get(c)  is null, c is not part of this Ship
     * if myPieces.get(c)  is false, c is part of this ship and has not been hit
     * if myPieces.get(c)  is true, c is part of this ship and has been hit
     */
    protected HashMap<Coordinate, Boolean> myPieces;
    protected TreeMap<Integer, Coordinate> orderedPieces;
    protected ShipDisplayInfo<T> myDisplayInfo;
    protected ShipDisplayInfo<T> enemyDisplayInfo;

    /**
     * Constructs a basic ship.
     *
     * @param orderedPieces    is the tree map contains the coordinates of the ship
     *                         with specific index
     * @param myDisplayInfo    is the display info of the ship
     * @param enemyDisplayInfo is the display info of the ship for enemy
     */
    public BasicShip(TreeMap<Integer, Coordinate> orderedPieces, ShipDisplayInfo<T> myDisplayInfo,
                     ShipDisplayInfo<T> enemyDisplayInfo) {
        this.myPieces = new HashMap<>();
        this.orderedPieces = orderedPieces;
        this.myDisplayInfo = myDisplayInfo;
        this.enemyDisplayInfo = enemyDisplayInfo;
        for (Coordinate coordinate : orderedPieces.values()) {
            myPieces.put(coordinate, false);
        }
    }

    /**
     * The abstraction of checking exception throwing.
     *
     * @param c is the coordinate
     */
    protected void checkCoordinateInThisShip(Coordinate c) {
        if (!occupiesCoordinates(c)) {
            throw new IllegalArgumentException("coordinate: " + c + "are not part of this ship.");
        }
    }

    /**
     * Check if this ship occupies the given coordinate.
     *
     * @param where is the Coordinate to check if this Ship occupies
     * @return true if where is inside this ship, false if not.
     */
    @Override
    public boolean occupiesCoordinates(Coordinate where) {
        return myPieces.containsKey(where);
    }

    /**
     * Check if this ship has been hit in all of its locations meaning it has been
     * sunk.
     *
     * @return true if this ship has been sunk, false otherwise.
     */
    @Override
    public boolean isSunk() {
        boolean isSunk = true;
        for (boolean b : myPieces.values()) {
            isSunk = isSunk & b;
        }
        return isSunk;
    }

    /**
     * Make this ship record that it has been hit at the given coordinate. The
     * specified coordinate must be part of the ship.
     *
     * @param where specifies the coordinates that were hit.
     * @throws IllegalArgumentException if where is not part of the Ship
     */
    @Override
    public void recordHitAt(Coordinate where) {
        checkCoordinateInThisShip(where);
        myPieces.put(where, true);
    }

    /**
     * Check if this ship was hit at the specified coordinates. The coordinates must
     * be part of this Ship.
     *
     * @param where is the coordinates to check.
     * @return true if this ship as hit at the indicated coordinates, and false
     * otherwise.
     * @throws IllegalArgumentException if the coordinates are not part of this
     *                                  ship.
     */
    @Override
    public boolean wasHitAt(Coordinate where) {
        checkCoordinateInThisShip(where);
        return myPieces.get(where);
    }

    /**
     * Return the view-specific information at the given coordinate. This coordinate
     * must be part of the ship.
     *
     * @param where  is the coordinate to return information for
     * @param myShip is whether the ship belongs to current player
     * @return The view-specific information at that coordinate.
     * @throws IllegalArgumentException if where is not part of the Ship
     */
    @Override
    public T getDisplayInfoAt(Coordinate where, boolean myShip) {
        checkCoordinateInThisShip(where);
        if (myShip) {
            return myDisplayInfo.getInfo(where, wasHitAt(where));
        }
        return enemyDisplayInfo.getInfo(where, wasHitAt(where));
    }

    /**
     * Get all the Coordinates that this Ship occupies.
     *
     * @return An Iterable with the coordinates that this Ship occupies
     */
    @Override
    public Iterable<Coordinate> getCoordinates() {
        return myPieces.keySet();
    }

    /**
     * Update the hit data of the ship from another ship.
     *
     * @param updateFrom the origin ship.
     */
    public void updatePieces(Ship<T> updateFrom) {
        for (Map.Entry<Integer, Coordinate> entry : orderedPieces.entrySet()) {
            this.myPieces.put(entry.getValue(),
                    updateFrom.wasHitAt(updateFrom.getOrderedPieces().get(entry.getKey())));
        }
    }

    /**
     * Get orderedPieces.
     *
     * @return orderedPieces.
     */
    public TreeMap<Integer, Coordinate> getOrderedPieces() {
        return orderedPieces;
    }

    /**
     * Override the equals function to compare two myShips list.
     *
     * @param o is the obj to compare.
     * @return true if the two obj are equal.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BasicShip<?> basicShip = (BasicShip<?>) o;
        return Objects.equals(myPieces, basicShip.myPieces);
    }

}
