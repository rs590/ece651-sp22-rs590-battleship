package edu.duke.rs590.battleship.client;

import edu.duke.rs590.battleship.Constants;
import edu.duke.rs590.battleship.Utility;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ClientApp {
    public static void main(String[] args) throws IOException, InterruptedException {
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
        System.out.print(Constants.WELCOME_MSG_V3 + Constants.NEW_LINE);
        Client client = new Client(Utility.readHostName(input), Utility.readPortNumForClient(input));
        System.out.println("Connected to " + client.getSocket().getInetAddress());
        client.choosePlayerType();
        client.start();
    }
}
