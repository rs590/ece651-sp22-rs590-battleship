package edu.duke.rs590.battleship;

/**
 * This interface handles the display info of the ship.
 */
public interface ShipDisplayInfo<T> {
    T getInfo(Coordinate where, boolean hit);
}
