# Battleship Game

*2022-Duke ECE651-Assignment1*

---

This project adopted an **incremental development strategy**
that followed the **open-close principle**, developing Version2 while ensuring the integrity of Version1.

The **default build target** is **Version 2** of the game.  
If you want to play version 1, please change the application block in app/build.gradle into:

````
application {
// Define the main class for the application.
mainClass = 'edu.duke.rs590.battleship.App'
}
````